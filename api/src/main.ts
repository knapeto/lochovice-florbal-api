import {
  BadRequestException,
  ValidationError,
  ValidationPipe,
} from '@nestjs/common';
import { json, urlencoded } from 'body-parser';

import { AppModule } from './app.module';
import { NestFactory } from '@nestjs/core';
import cookieParser from 'cookie-parser';
import { graphqlUploadExpress } from 'graphql-upload';
import { values } from 'ramda';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    allowedHeaders: "*",
    origin: "*",
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });
  app.use(cookieParser());

  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (validationErrors: ValidationError[] = []) => {
        const errorResult = {};
        validationErrors.map((error) => {
          errorResult[error.property] = values(error.constraints);
        });
        return new BadRequestException(errorResult);
      },
    }),
  );

  app.use(json({ limit: '500mb' }));
  app.use(
    urlencoded({
      limit: '500mb',
      extended: true,
      parameterLimit: 10000000000,
    }),
  );
  app.use(graphqlUploadExpress({ maxFileSize: 10000000000, maxFiles: 10 }));
  console.log(process.env.PORT || 3000)
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
