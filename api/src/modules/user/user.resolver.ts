import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UserService } from './user.service';
import { User } from '../../@generated/user/user.model';
import { Authorize, GqlUser, ResGql } from '../core/auth.decorator';
import { LoginResponse } from './dto/login.dto';
import { SharedService } from '../shared/shared.service';
import { AuthType } from '../core/role.guard';
import { Response } from 'express';

@Resolver()
export class UserResolver {
  constructor(
    private readonly userService: UserService,
    private readonly sharedService: SharedService,
  ) {}

  @Authorize()
  @Query(() => User)
  async me(@GqlUser() user: User): Promise<User> {
    return await this.userService.getUserById(user.id);
  }

  @Mutation(() => LoginResponse)
  async login(
    @Args('email') email: string,
    @Args('password') password: string,
    @ResGql() res: Response,
  ): Promise<LoginResponse> {
    return await this.sharedService.login(email, password, AuthType.User, res);
  }
}
