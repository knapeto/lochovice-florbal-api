import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from '../prisma/prisma.service';
import { User } from '../../@generated/user/user.model';

@Injectable()
export class UserService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly jwtService: JwtService,
  ) { }

  async getUserById(userId: string): Promise<User> {
    return this.prisma.user.findFirst({
      where: {
        id: userId,
      },
    });
  }
}
