import { Mutation, Query, Resolver } from 'type-graphql';

@Resolver()
export class AdminResolver {
  @Query(() => Boolean)
  async testQuery() {
    return true;
  }

  @Mutation(() => Boolean)
  async testMutation() {
    return true;
  }
}
