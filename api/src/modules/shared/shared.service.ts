import { Injectable, UnauthorizedException } from '@nestjs/common';

import { AuthType } from '../core/role.guard';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from '../prisma/prisma.service';
import { Response } from 'express';
import { User } from '../user/entities/user.entity';
import { isPasswordValid } from 'src/utils/password';

@Injectable()
export class SharedService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly prismaService: PrismaService,
  ) { }

  static getUserDataForToken(user: User, role: AuthType) {
    return {
      id: user.id,
      tokenSalt: user.tokenSalt,
      role,
    };
  }

  async login(email: string, password: string, role: AuthType, res: Response) {
    const user = await this.prismaService.user.findFirst({
      where: {
        email,
      },
    });

    if (!user) {
      throw new UnauthorizedException('LOGIN_ERROR');
    }

    if (user && !user.isAdmin && role === AuthType.Admin) {
      throw new UnauthorizedException('LOGIN_ERROR');
    }

    if (!isPasswordValid(password, user.password)) {
      throw new UnauthorizedException('LOGIN_ERROR');
    }

    const access_token = this.jwtService.sign(
      SharedService.getUserDataForToken(user, role),
    );

    res &&
      res.cookie('access_token', `${access_token}`, {
        httpOnly: true,
        secure: false,
      });

    return {
      user,
      access_token,
    };
  }
}
