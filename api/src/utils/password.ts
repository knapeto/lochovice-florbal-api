import bcrypt from 'bcryptjs';

export function hashPassword(plaintextPassword: string): string {
  const saltRounds = 10;
  return bcrypt.hashSync(plaintextPassword, saltRounds);
}

export function getTokenSalt(): string {
  const saltRounds = 10;
  return bcrypt.hashSync(new Date().getTime().toString(), saltRounds);
}

export function isPasswordValid(
  plaintextPassword: string,
  passwordHash: string,
) {
  return bcrypt.compareSync(plaintextPassword, passwordHash);
}
