import { getTokenSalt, hashPassword } from '../src/utils/password';

import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const seeds = async () => {
	await prisma.user.create({
		data: {
			email: 'knapeto@gmail.com',
			firstName: 'Admin',
			lastName: 'Admin',
			password: hashPassword('test'),
			tokenSalt: getTokenSalt(),
			isAdmin: true,
		},
	});

	return true;
};

export default seeds();