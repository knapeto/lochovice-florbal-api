import { DELETE, UPDATE } from 'react-admin';
// import buildGraphQLProvider, { Options } from 'ra-data-graphql';
import { Options, buildDataProvider } from '@ra-data-prisma/dataprovider';
import { useEffect, useState } from 'react';

import withAuthProvider from './authProvider';

export const appProvidersFactory = () => {
  return (options: Options) => {
    const [dataProvider, setDataProvider] = useState<any>();
    const authProvider = withAuthProvider();

    useEffect(() => {
      buildDataProvider(options)
        .then((p) => {
          setDataProvider(() => p);
        })

        .catch((e) => {
          console.error(e);
        });
    }, []);

    const getDataProvider = (type: string, resource: string, params: any) => {
      if (!dataProvider) return false;

      if (
        type === DELETE &&
        ['User', 'Category', 'Product', 'Extra'].includes(resource)
      ) {
        return dataProvider(UPDATE, resource, {
          data: {
            id: params.id,
            deletedAt: new Date(),
          },
        });
      }

      return dataProvider(type, resource, params);
    };

    return {
      dataProvider: dataProvider && getDataProvider,
      authProvider,
    };
  };
};
