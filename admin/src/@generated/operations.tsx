import * as Types from './schemas';

export type TestMutationMutationVariables = Types.Exact<{
  [key: string]: never;
}>;

export type TestMutationMutation = {
  __typename?: 'Mutation';
  testMutation: boolean;
};

export type TestQueryQueryVariables = Types.Exact<{ [key: string]: never }>;

export type TestQueryQuery = { __typename?: 'Query'; testQuery: boolean };
