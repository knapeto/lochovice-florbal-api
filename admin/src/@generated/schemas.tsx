export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
export type MakeEmpty<
  T extends { [key: string]: unknown },
  K extends keyof T,
> = { [_ in K]?: never };
export type Incremental<T> =
  | T
  | {
      [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never;
    };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string };
  String: { input: string; output: string };
  Boolean: { input: boolean; output: boolean };
  Int: { input: number; output: number };
  Float: { input: number; output: number };
  DateTimeISO: { input: any; output: any };
};

export type AffectedRowsOutput = {
  __typename?: 'AffectedRowsOutput';
  count: Scalars['Int']['output'];
};

export type AggregateEvent = {
  __typename?: 'AggregateEvent';
  _avg?: Maybe<EventAvgAggregate>;
  _count?: Maybe<EventCountAggregate>;
  _max?: Maybe<EventMaxAggregate>;
  _min?: Maybe<EventMinAggregate>;
  _sum?: Maybe<EventSumAggregate>;
};

export type AggregateExpense = {
  __typename?: 'AggregateExpense';
  _avg?: Maybe<ExpenseAvgAggregate>;
  _count?: Maybe<ExpenseCountAggregate>;
  _max?: Maybe<ExpenseMaxAggregate>;
  _min?: Maybe<ExpenseMinAggregate>;
  _sum?: Maybe<ExpenseSumAggregate>;
};

export type AggregateUser = {
  __typename?: 'AggregateUser';
  _count?: Maybe<UserCountAggregate>;
  _max?: Maybe<UserMaxAggregate>;
  _min?: Maybe<UserMinAggregate>;
};

export type AggregateUserAttendace = {
  __typename?: 'AggregateUserAttendace';
  _avg?: Maybe<UserAttendaceAvgAggregate>;
  _count?: Maybe<UserAttendaceCountAggregate>;
  _max?: Maybe<UserAttendaceMaxAggregate>;
  _min?: Maybe<UserAttendaceMinAggregate>;
  _sum?: Maybe<UserAttendaceSumAggregate>;
};

export type AggregateWallet = {
  __typename?: 'AggregateWallet';
  _avg?: Maybe<WalletAvgAggregate>;
  _count?: Maybe<WalletCountAggregate>;
  _max?: Maybe<WalletMaxAggregate>;
  _min?: Maybe<WalletMinAggregate>;
  _sum?: Maybe<WalletSumAggregate>;
};

export type BoolFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['Boolean']['input']>;
};

export type BoolFilter = {
  equals?: InputMaybe<Scalars['Boolean']['input']>;
  not?: InputMaybe<NestedBoolFilter>;
};

export type BoolWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedBoolFilter>;
  _min?: InputMaybe<NestedBoolFilter>;
  equals?: InputMaybe<Scalars['Boolean']['input']>;
  not?: InputMaybe<NestedBoolWithAggregatesFilter>;
};

export type DateTimeFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['DateTimeISO']['input']>;
};

export type DateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  in?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
  lt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  lte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  not?: InputMaybe<NestedDateTimeFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
};

export type DateTimeNullableFilter = {
  equals?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  in?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
  lt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  lte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  not?: InputMaybe<NestedDateTimeNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
};

export type DateTimeNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedDateTimeNullableFilter>;
  _min?: InputMaybe<NestedDateTimeNullableFilter>;
  equals?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  in?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
  lt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  lte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  not?: InputMaybe<NestedDateTimeNullableWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
};

export type DateTimeWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedDateTimeFilter>;
  _min?: InputMaybe<NestedDateTimeFilter>;
  equals?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  in?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
  lt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  lte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  not?: InputMaybe<NestedDateTimeWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
};

export type Event = {
  __typename?: 'Event';
  _count?: Maybe<EventCount>;
  createdAt: Scalars['DateTimeISO']['output'];
  deletedAt?: Maybe<Scalars['DateTimeISO']['output']>;
  expense: Scalars['Float']['output'];
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  users: Array<UserAttendace>;
};

export type EventUsersArgs = {
  cursor?: InputMaybe<UserAttendaceWhereUniqueInput>;
  distinct?: InputMaybe<Array<UserAttendaceScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<UserAttendaceOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type EventAvgAggregate = {
  __typename?: 'EventAvgAggregate';
  expense?: Maybe<Scalars['Float']['output']>;
};

export type EventAvgOrderByAggregateInput = {
  expense?: InputMaybe<SortOrder>;
};

export type EventCount = {
  __typename?: 'EventCount';
  users: Scalars['Int']['output'];
};

export type EventCountUsersArgs = {
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type EventCountAggregate = {
  __typename?: 'EventCountAggregate';
  _all: Scalars['Int']['output'];
  createdAt: Scalars['Int']['output'];
  deletedAt: Scalars['Int']['output'];
  expense: Scalars['Int']['output'];
  id: Scalars['Int']['output'];
  name: Scalars['Int']['output'];
};

export type EventCountOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  deletedAt?: InputMaybe<SortOrder>;
  expense?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
};

export type EventCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  deletedAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  expense: Scalars['Float']['input'];
  id?: InputMaybe<Scalars['String']['input']>;
  name: Scalars['String']['input'];
  users?: InputMaybe<UserAttendaceCreateNestedManyWithoutEventInput>;
};

export type EventCreateManyInput = {
  createdAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  deletedAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  expense: Scalars['Float']['input'];
  id?: InputMaybe<Scalars['String']['input']>;
  name: Scalars['String']['input'];
};

export type EventCreateNestedOneWithoutUsersInput = {
  connect?: InputMaybe<EventWhereUniqueInput>;
  connectOrCreate?: InputMaybe<EventCreateOrConnectWithoutUsersInput>;
  create?: InputMaybe<EventCreateWithoutUsersInput>;
};

export type EventCreateOrConnectWithoutUsersInput = {
  create: EventCreateWithoutUsersInput;
  where: EventWhereUniqueInput;
};

export type EventCreateWithoutUsersInput = {
  createdAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  deletedAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  expense: Scalars['Float']['input'];
  id?: InputMaybe<Scalars['String']['input']>;
  name: Scalars['String']['input'];
};

export type EventGroupBy = {
  __typename?: 'EventGroupBy';
  _avg?: Maybe<EventAvgAggregate>;
  _count?: Maybe<EventCountAggregate>;
  _max?: Maybe<EventMaxAggregate>;
  _min?: Maybe<EventMinAggregate>;
  _sum?: Maybe<EventSumAggregate>;
  createdAt: Scalars['DateTimeISO']['output'];
  deletedAt?: Maybe<Scalars['DateTimeISO']['output']>;
  expense: Scalars['Float']['output'];
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
};

export type EventMaxAggregate = {
  __typename?: 'EventMaxAggregate';
  createdAt?: Maybe<Scalars['DateTimeISO']['output']>;
  deletedAt?: Maybe<Scalars['DateTimeISO']['output']>;
  expense?: Maybe<Scalars['Float']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
};

export type EventMaxOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  deletedAt?: InputMaybe<SortOrder>;
  expense?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
};

export type EventMinAggregate = {
  __typename?: 'EventMinAggregate';
  createdAt?: Maybe<Scalars['DateTimeISO']['output']>;
  deletedAt?: Maybe<Scalars['DateTimeISO']['output']>;
  expense?: Maybe<Scalars['Float']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
};

export type EventMinOrderByAggregateInput = {
  createdAt?: InputMaybe<SortOrder>;
  deletedAt?: InputMaybe<SortOrder>;
  expense?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
};

export type EventOrderByWithAggregationInput = {
  _avg?: InputMaybe<EventAvgOrderByAggregateInput>;
  _count?: InputMaybe<EventCountOrderByAggregateInput>;
  _max?: InputMaybe<EventMaxOrderByAggregateInput>;
  _min?: InputMaybe<EventMinOrderByAggregateInput>;
  _sum?: InputMaybe<EventSumOrderByAggregateInput>;
  createdAt?: InputMaybe<SortOrder>;
  deletedAt?: InputMaybe<SortOrderInput>;
  expense?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
};

export type EventOrderByWithRelationInput = {
  createdAt?: InputMaybe<SortOrder>;
  deletedAt?: InputMaybe<SortOrderInput>;
  expense?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  users?: InputMaybe<UserAttendaceOrderByRelationAggregateInput>;
};

export type EventRelationFilter = {
  is?: InputMaybe<EventWhereInput>;
  isNot?: InputMaybe<EventWhereInput>;
};

export enum EventScalarFieldEnum {
  CreatedAt = 'createdAt',
  DeletedAt = 'deletedAt',
  Expense = 'expense',
  Id = 'id',
  Name = 'name',
}

export type EventScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<EventScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<EventScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<EventScalarWhereWithAggregatesInput>>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  deletedAt?: InputMaybe<DateTimeNullableWithAggregatesFilter>;
  expense?: InputMaybe<FloatWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  name?: InputMaybe<StringWithAggregatesFilter>;
};

export type EventSumAggregate = {
  __typename?: 'EventSumAggregate';
  expense?: Maybe<Scalars['Float']['output']>;
};

export type EventSumOrderByAggregateInput = {
  expense?: InputMaybe<SortOrder>;
};

export type EventUpdateInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  deletedAt?: InputMaybe<NullableDateTimeFieldUpdateOperationsInput>;
  expense?: InputMaybe<FloatFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  users?: InputMaybe<UserAttendaceUpdateManyWithoutEventNestedInput>;
};

export type EventUpdateManyMutationInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  deletedAt?: InputMaybe<NullableDateTimeFieldUpdateOperationsInput>;
  expense?: InputMaybe<FloatFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type EventUpdateOneRequiredWithoutUsersNestedInput = {
  connect?: InputMaybe<EventWhereUniqueInput>;
  connectOrCreate?: InputMaybe<EventCreateOrConnectWithoutUsersInput>;
  create?: InputMaybe<EventCreateWithoutUsersInput>;
  update?: InputMaybe<EventUpdateToOneWithWhereWithoutUsersInput>;
  upsert?: InputMaybe<EventUpsertWithoutUsersInput>;
};

export type EventUpdateToOneWithWhereWithoutUsersInput = {
  data: EventUpdateWithoutUsersInput;
  where?: InputMaybe<EventWhereInput>;
};

export type EventUpdateWithoutUsersInput = {
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  deletedAt?: InputMaybe<NullableDateTimeFieldUpdateOperationsInput>;
  expense?: InputMaybe<FloatFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type EventUpsertWithoutUsersInput = {
  create: EventCreateWithoutUsersInput;
  update: EventUpdateWithoutUsersInput;
  where?: InputMaybe<EventWhereInput>;
};

export type EventWhereInput = {
  AND?: InputMaybe<Array<EventWhereInput>>;
  NOT?: InputMaybe<Array<EventWhereInput>>;
  OR?: InputMaybe<Array<EventWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  deletedAt?: InputMaybe<DateTimeNullableFilter>;
  expense?: InputMaybe<FloatFilter>;
  id?: InputMaybe<StringFilter>;
  name?: InputMaybe<StringFilter>;
  users?: InputMaybe<UserAttendaceListRelationFilter>;
};

export type EventWhereUniqueInput = {
  AND?: InputMaybe<Array<EventWhereInput>>;
  NOT?: InputMaybe<Array<EventWhereInput>>;
  OR?: InputMaybe<Array<EventWhereInput>>;
  createdAt?: InputMaybe<DateTimeFilter>;
  deletedAt?: InputMaybe<DateTimeNullableFilter>;
  expense?: InputMaybe<FloatFilter>;
  id?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<StringFilter>;
  users?: InputMaybe<UserAttendaceListRelationFilter>;
};

export type Expense = {
  __typename?: 'Expense';
  amount: Scalars['Float']['output'];
  createdAt: Scalars['DateTimeISO']['output'];
  deletedAt?: Maybe<Scalars['DateTimeISO']['output']>;
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  splitCount: Scalars['Float']['output'];
};

export type ExpenseAvgAggregate = {
  __typename?: 'ExpenseAvgAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
  splitCount?: Maybe<Scalars['Float']['output']>;
};

export type ExpenseAvgOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  splitCount?: InputMaybe<SortOrder>;
};

export type ExpenseCountAggregate = {
  __typename?: 'ExpenseCountAggregate';
  _all: Scalars['Int']['output'];
  amount: Scalars['Int']['output'];
  createdAt: Scalars['Int']['output'];
  deletedAt: Scalars['Int']['output'];
  id: Scalars['Int']['output'];
  name: Scalars['Int']['output'];
  splitCount: Scalars['Int']['output'];
};

export type ExpenseCountOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  deletedAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  splitCount?: InputMaybe<SortOrder>;
};

export type ExpenseCreateInput = {
  amount: Scalars['Float']['input'];
  createdAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  deletedAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  name: Scalars['String']['input'];
  splitCount: Scalars['Float']['input'];
};

export type ExpenseCreateManyInput = {
  amount: Scalars['Float']['input'];
  createdAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  deletedAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  name: Scalars['String']['input'];
  splitCount: Scalars['Float']['input'];
};

export type ExpenseGroupBy = {
  __typename?: 'ExpenseGroupBy';
  _avg?: Maybe<ExpenseAvgAggregate>;
  _count?: Maybe<ExpenseCountAggregate>;
  _max?: Maybe<ExpenseMaxAggregate>;
  _min?: Maybe<ExpenseMinAggregate>;
  _sum?: Maybe<ExpenseSumAggregate>;
  amount: Scalars['Float']['output'];
  createdAt: Scalars['DateTimeISO']['output'];
  deletedAt?: Maybe<Scalars['DateTimeISO']['output']>;
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  splitCount: Scalars['Float']['output'];
};

export type ExpenseMaxAggregate = {
  __typename?: 'ExpenseMaxAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
  createdAt?: Maybe<Scalars['DateTimeISO']['output']>;
  deletedAt?: Maybe<Scalars['DateTimeISO']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  splitCount?: Maybe<Scalars['Float']['output']>;
};

export type ExpenseMaxOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  deletedAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  splitCount?: InputMaybe<SortOrder>;
};

export type ExpenseMinAggregate = {
  __typename?: 'ExpenseMinAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
  createdAt?: Maybe<Scalars['DateTimeISO']['output']>;
  deletedAt?: Maybe<Scalars['DateTimeISO']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  name?: Maybe<Scalars['String']['output']>;
  splitCount?: Maybe<Scalars['Float']['output']>;
};

export type ExpenseMinOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  deletedAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  splitCount?: InputMaybe<SortOrder>;
};

export type ExpenseOrderByWithAggregationInput = {
  _avg?: InputMaybe<ExpenseAvgOrderByAggregateInput>;
  _count?: InputMaybe<ExpenseCountOrderByAggregateInput>;
  _max?: InputMaybe<ExpenseMaxOrderByAggregateInput>;
  _min?: InputMaybe<ExpenseMinOrderByAggregateInput>;
  _sum?: InputMaybe<ExpenseSumOrderByAggregateInput>;
  amount?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  deletedAt?: InputMaybe<SortOrderInput>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  splitCount?: InputMaybe<SortOrder>;
};

export type ExpenseOrderByWithRelationInput = {
  amount?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  deletedAt?: InputMaybe<SortOrderInput>;
  id?: InputMaybe<SortOrder>;
  name?: InputMaybe<SortOrder>;
  splitCount?: InputMaybe<SortOrder>;
};

export enum ExpenseScalarFieldEnum {
  Amount = 'amount',
  CreatedAt = 'createdAt',
  DeletedAt = 'deletedAt',
  Id = 'id',
  Name = 'name',
  SplitCount = 'splitCount',
}

export type ExpenseScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<ExpenseScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<ExpenseScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<ExpenseScalarWhereWithAggregatesInput>>;
  amount?: InputMaybe<FloatWithAggregatesFilter>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  deletedAt?: InputMaybe<DateTimeNullableWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  name?: InputMaybe<StringWithAggregatesFilter>;
  splitCount?: InputMaybe<FloatWithAggregatesFilter>;
};

export type ExpenseSumAggregate = {
  __typename?: 'ExpenseSumAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
  splitCount?: Maybe<Scalars['Float']['output']>;
};

export type ExpenseSumOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  splitCount?: InputMaybe<SortOrder>;
};

export type ExpenseUpdateInput = {
  amount?: InputMaybe<FloatFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  deletedAt?: InputMaybe<NullableDateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  splitCount?: InputMaybe<FloatFieldUpdateOperationsInput>;
};

export type ExpenseUpdateManyMutationInput = {
  amount?: InputMaybe<FloatFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  deletedAt?: InputMaybe<NullableDateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  name?: InputMaybe<StringFieldUpdateOperationsInput>;
  splitCount?: InputMaybe<FloatFieldUpdateOperationsInput>;
};

export type ExpenseWhereInput = {
  AND?: InputMaybe<Array<ExpenseWhereInput>>;
  NOT?: InputMaybe<Array<ExpenseWhereInput>>;
  OR?: InputMaybe<Array<ExpenseWhereInput>>;
  amount?: InputMaybe<FloatFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  deletedAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<StringFilter>;
  name?: InputMaybe<StringFilter>;
  splitCount?: InputMaybe<FloatFilter>;
};

export type ExpenseWhereUniqueInput = {
  AND?: InputMaybe<Array<ExpenseWhereInput>>;
  NOT?: InputMaybe<Array<ExpenseWhereInput>>;
  OR?: InputMaybe<Array<ExpenseWhereInput>>;
  amount?: InputMaybe<FloatFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  deletedAt?: InputMaybe<DateTimeNullableFilter>;
  id?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<StringFilter>;
  splitCount?: InputMaybe<FloatFilter>;
};

export type FloatFieldUpdateOperationsInput = {
  decrement?: InputMaybe<Scalars['Float']['input']>;
  divide?: InputMaybe<Scalars['Float']['input']>;
  increment?: InputMaybe<Scalars['Float']['input']>;
  multiply?: InputMaybe<Scalars['Float']['input']>;
  set?: InputMaybe<Scalars['Float']['input']>;
};

export type FloatFilter = {
  equals?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<NestedFloatFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']['input']>>;
};

export type FloatNullableFilter = {
  equals?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<NestedFloatNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']['input']>>;
};

export type FloatNullableWithAggregatesFilter = {
  _avg?: InputMaybe<NestedFloatNullableFilter>;
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedFloatNullableFilter>;
  _min?: InputMaybe<NestedFloatNullableFilter>;
  _sum?: InputMaybe<NestedFloatNullableFilter>;
  equals?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<NestedFloatNullableWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']['input']>>;
};

export type FloatWithAggregatesFilter = {
  _avg?: InputMaybe<NestedFloatFilter>;
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedFloatFilter>;
  _min?: InputMaybe<NestedFloatFilter>;
  _sum?: InputMaybe<NestedFloatFilter>;
  equals?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<NestedFloatWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']['input']>>;
};

export type LoginResponse = {
  __typename?: 'LoginResponse';
  access_token: Scalars['String']['output'];
  user: User;
};

export type Mutation = {
  __typename?: 'Mutation';
  createManyEvent: AffectedRowsOutput;
  createManyExpense: AffectedRowsOutput;
  createManyUser: AffectedRowsOutput;
  createManyUserAttendace: AffectedRowsOutput;
  createManyWallet: AffectedRowsOutput;
  createOneEvent: Event;
  createOneExpense: Expense;
  createOneUser: User;
  createOneUserAttendace: UserAttendace;
  createOneWallet: Wallet;
  deleteManyEvent: AffectedRowsOutput;
  deleteManyExpense: AffectedRowsOutput;
  deleteManyUser: AffectedRowsOutput;
  deleteManyUserAttendace: AffectedRowsOutput;
  deleteManyWallet: AffectedRowsOutput;
  deleteOneEvent?: Maybe<Event>;
  deleteOneExpense?: Maybe<Expense>;
  deleteOneUser?: Maybe<User>;
  deleteOneUserAttendace?: Maybe<UserAttendace>;
  deleteOneWallet?: Maybe<Wallet>;
  login: LoginResponse;
  testMutation: Scalars['Boolean']['output'];
  updateManyEvent: AffectedRowsOutput;
  updateManyExpense: AffectedRowsOutput;
  updateManyUser: AffectedRowsOutput;
  updateManyUserAttendace: AffectedRowsOutput;
  updateManyWallet: AffectedRowsOutput;
  updateOneEvent?: Maybe<Event>;
  updateOneExpense?: Maybe<Expense>;
  updateOneUser?: Maybe<User>;
  updateOneUserAttendace?: Maybe<UserAttendace>;
  updateOneWallet?: Maybe<Wallet>;
  upsertOneEvent: Event;
  upsertOneExpense: Expense;
  upsertOneUser: User;
  upsertOneUserAttendace: UserAttendace;
  upsertOneWallet: Wallet;
};

export type MutationCreateManyEventArgs = {
  data: Array<EventCreateManyInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationCreateManyExpenseArgs = {
  data: Array<ExpenseCreateManyInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationCreateManyUserArgs = {
  data: Array<UserCreateManyInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationCreateManyUserAttendaceArgs = {
  data: Array<UserAttendaceCreateManyInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationCreateManyWalletArgs = {
  data: Array<WalletCreateManyInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']['input']>;
};

export type MutationCreateOneEventArgs = {
  data: EventCreateInput;
};

export type MutationCreateOneExpenseArgs = {
  data: ExpenseCreateInput;
};

export type MutationCreateOneUserArgs = {
  data?: InputMaybe<UserCreateInput>;
};

export type MutationCreateOneUserAttendaceArgs = {
  data: UserAttendaceCreateInput;
};

export type MutationCreateOneWalletArgs = {
  data: WalletCreateInput;
};

export type MutationDeleteManyEventArgs = {
  where?: InputMaybe<EventWhereInput>;
};

export type MutationDeleteManyExpenseArgs = {
  where?: InputMaybe<ExpenseWhereInput>;
};

export type MutationDeleteManyUserArgs = {
  where?: InputMaybe<UserWhereInput>;
};

export type MutationDeleteManyUserAttendaceArgs = {
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type MutationDeleteManyWalletArgs = {
  where?: InputMaybe<WalletWhereInput>;
};

export type MutationDeleteOneEventArgs = {
  where: EventWhereUniqueInput;
};

export type MutationDeleteOneExpenseArgs = {
  where: ExpenseWhereUniqueInput;
};

export type MutationDeleteOneUserArgs = {
  where: UserWhereUniqueInput;
};

export type MutationDeleteOneUserAttendaceArgs = {
  where: UserAttendaceWhereUniqueInput;
};

export type MutationDeleteOneWalletArgs = {
  where: WalletWhereUniqueInput;
};

export type MutationLoginArgs = {
  email: Scalars['String']['input'];
  password: Scalars['String']['input'];
};

export type MutationUpdateManyEventArgs = {
  data: EventUpdateManyMutationInput;
  where?: InputMaybe<EventWhereInput>;
};

export type MutationUpdateManyExpenseArgs = {
  data: ExpenseUpdateManyMutationInput;
  where?: InputMaybe<ExpenseWhereInput>;
};

export type MutationUpdateManyUserArgs = {
  data: UserUpdateManyMutationInput;
  where?: InputMaybe<UserWhereInput>;
};

export type MutationUpdateManyUserAttendaceArgs = {
  data: UserAttendaceUpdateManyMutationInput;
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type MutationUpdateManyWalletArgs = {
  data: WalletUpdateManyMutationInput;
  where?: InputMaybe<WalletWhereInput>;
};

export type MutationUpdateOneEventArgs = {
  data: EventUpdateInput;
  where: EventWhereUniqueInput;
};

export type MutationUpdateOneExpenseArgs = {
  data: ExpenseUpdateInput;
  where: ExpenseWhereUniqueInput;
};

export type MutationUpdateOneUserArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};

export type MutationUpdateOneUserAttendaceArgs = {
  data: UserAttendaceUpdateInput;
  where: UserAttendaceWhereUniqueInput;
};

export type MutationUpdateOneWalletArgs = {
  data: WalletUpdateInput;
  where: WalletWhereUniqueInput;
};

export type MutationUpsertOneEventArgs = {
  create: EventCreateInput;
  update: EventUpdateInput;
  where: EventWhereUniqueInput;
};

export type MutationUpsertOneExpenseArgs = {
  create: ExpenseCreateInput;
  update: ExpenseUpdateInput;
  where: ExpenseWhereUniqueInput;
};

export type MutationUpsertOneUserArgs = {
  create: UserCreateInput;
  update: UserUpdateInput;
  where: UserWhereUniqueInput;
};

export type MutationUpsertOneUserAttendaceArgs = {
  create: UserAttendaceCreateInput;
  update: UserAttendaceUpdateInput;
  where: UserAttendaceWhereUniqueInput;
};

export type MutationUpsertOneWalletArgs = {
  create: WalletCreateInput;
  update: WalletUpdateInput;
  where: WalletWhereUniqueInput;
};

export type NestedBoolFilter = {
  equals?: InputMaybe<Scalars['Boolean']['input']>;
  not?: InputMaybe<NestedBoolFilter>;
};

export type NestedBoolWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedBoolFilter>;
  _min?: InputMaybe<NestedBoolFilter>;
  equals?: InputMaybe<Scalars['Boolean']['input']>;
  not?: InputMaybe<NestedBoolWithAggregatesFilter>;
};

export type NestedDateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  in?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
  lt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  lte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  not?: InputMaybe<NestedDateTimeFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
};

export type NestedDateTimeNullableFilter = {
  equals?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  in?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
  lt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  lte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  not?: InputMaybe<NestedDateTimeNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
};

export type NestedDateTimeNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedDateTimeNullableFilter>;
  _min?: InputMaybe<NestedDateTimeNullableFilter>;
  equals?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  in?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
  lt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  lte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  not?: InputMaybe<NestedDateTimeNullableWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
};

export type NestedDateTimeWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedDateTimeFilter>;
  _min?: InputMaybe<NestedDateTimeFilter>;
  equals?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  gte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  in?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
  lt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  lte?: InputMaybe<Scalars['DateTimeISO']['input']>;
  not?: InputMaybe<NestedDateTimeWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTimeISO']['input']>>;
};

export type NestedFloatFilter = {
  equals?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<NestedFloatFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']['input']>>;
};

export type NestedFloatNullableFilter = {
  equals?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<NestedFloatNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']['input']>>;
};

export type NestedFloatNullableWithAggregatesFilter = {
  _avg?: InputMaybe<NestedFloatNullableFilter>;
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedFloatNullableFilter>;
  _min?: InputMaybe<NestedFloatNullableFilter>;
  _sum?: InputMaybe<NestedFloatNullableFilter>;
  equals?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<NestedFloatNullableWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']['input']>>;
};

export type NestedFloatWithAggregatesFilter = {
  _avg?: InputMaybe<NestedFloatFilter>;
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedFloatFilter>;
  _min?: InputMaybe<NestedFloatFilter>;
  _sum?: InputMaybe<NestedFloatFilter>;
  equals?: InputMaybe<Scalars['Float']['input']>;
  gt?: InputMaybe<Scalars['Float']['input']>;
  gte?: InputMaybe<Scalars['Float']['input']>;
  in?: InputMaybe<Array<Scalars['Float']['input']>>;
  lt?: InputMaybe<Scalars['Float']['input']>;
  lte?: InputMaybe<Scalars['Float']['input']>;
  not?: InputMaybe<NestedFloatWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']['input']>>;
};

export type NestedIntFilter = {
  equals?: InputMaybe<Scalars['Int']['input']>;
  gt?: InputMaybe<Scalars['Int']['input']>;
  gte?: InputMaybe<Scalars['Int']['input']>;
  in?: InputMaybe<Array<Scalars['Int']['input']>>;
  lt?: InputMaybe<Scalars['Int']['input']>;
  lte?: InputMaybe<Scalars['Int']['input']>;
  not?: InputMaybe<NestedIntFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']['input']>>;
};

export type NestedIntNullableFilter = {
  equals?: InputMaybe<Scalars['Int']['input']>;
  gt?: InputMaybe<Scalars['Int']['input']>;
  gte?: InputMaybe<Scalars['Int']['input']>;
  in?: InputMaybe<Array<Scalars['Int']['input']>>;
  lt?: InputMaybe<Scalars['Int']['input']>;
  lte?: InputMaybe<Scalars['Int']['input']>;
  not?: InputMaybe<NestedIntNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['Int']['input']>>;
};

export type NestedStringFilter = {
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  not?: InputMaybe<NestedStringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type NestedStringNullableFilter = {
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  not?: InputMaybe<NestedStringNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type NestedStringNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedStringNullableFilter>;
  _min?: InputMaybe<NestedStringNullableFilter>;
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  not?: InputMaybe<NestedStringNullableWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type NestedStringWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedStringFilter>;
  _min?: InputMaybe<NestedStringFilter>;
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  not?: InputMaybe<NestedStringWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type NullableDateTimeFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['DateTimeISO']['input']>;
};

export type NullableFloatFieldUpdateOperationsInput = {
  decrement?: InputMaybe<Scalars['Float']['input']>;
  divide?: InputMaybe<Scalars['Float']['input']>;
  increment?: InputMaybe<Scalars['Float']['input']>;
  multiply?: InputMaybe<Scalars['Float']['input']>;
  set?: InputMaybe<Scalars['Float']['input']>;
};

export type NullableStringFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['String']['input']>;
};

export enum NullsOrder {
  First = 'first',
  Last = 'last',
}

export type Query = {
  __typename?: 'Query';
  aggregateEvent: AggregateEvent;
  aggregateExpense: AggregateExpense;
  aggregateUser: AggregateUser;
  aggregateUserAttendace: AggregateUserAttendace;
  aggregateWallet: AggregateWallet;
  event?: Maybe<Event>;
  events: Array<Event>;
  expense?: Maybe<Expense>;
  expenses: Array<Expense>;
  findFirstEvent?: Maybe<Event>;
  findFirstEventOrThrow?: Maybe<Event>;
  findFirstExpense?: Maybe<Expense>;
  findFirstExpenseOrThrow?: Maybe<Expense>;
  findFirstUser?: Maybe<User>;
  findFirstUserAttendace?: Maybe<UserAttendace>;
  findFirstUserAttendaceOrThrow?: Maybe<UserAttendace>;
  findFirstUserOrThrow?: Maybe<User>;
  findFirstWallet?: Maybe<Wallet>;
  findFirstWalletOrThrow?: Maybe<Wallet>;
  getEvent?: Maybe<Event>;
  getExpense?: Maybe<Expense>;
  getUser?: Maybe<User>;
  getUserAttendace?: Maybe<UserAttendace>;
  getWallet?: Maybe<Wallet>;
  groupByEvent: Array<EventGroupBy>;
  groupByExpense: Array<ExpenseGroupBy>;
  groupByUser: Array<UserGroupBy>;
  groupByUserAttendace: Array<UserAttendaceGroupBy>;
  groupByWallet: Array<WalletGroupBy>;
  testQuery: Scalars['Boolean']['output'];
  user?: Maybe<User>;
  userAttendace?: Maybe<UserAttendace>;
  userAttendaces: Array<UserAttendace>;
  users: Array<User>;
  wallet?: Maybe<Wallet>;
  wallets: Array<Wallet>;
};

export type QueryAggregateEventArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  orderBy?: InputMaybe<Array<EventOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<EventWhereInput>;
};

export type QueryAggregateExpenseArgs = {
  cursor?: InputMaybe<ExpenseWhereUniqueInput>;
  orderBy?: InputMaybe<Array<ExpenseOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ExpenseWhereInput>;
};

export type QueryAggregateUserArgs = {
  cursor?: InputMaybe<UserWhereUniqueInput>;
  orderBy?: InputMaybe<Array<UserOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserWhereInput>;
};

export type QueryAggregateUserAttendaceArgs = {
  cursor?: InputMaybe<UserAttendaceWhereUniqueInput>;
  orderBy?: InputMaybe<Array<UserAttendaceOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type QueryAggregateWalletArgs = {
  cursor?: InputMaybe<WalletWhereUniqueInput>;
  orderBy?: InputMaybe<Array<WalletOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<WalletWhereInput>;
};

export type QueryEventArgs = {
  where: EventWhereUniqueInput;
};

export type QueryEventsArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  distinct?: InputMaybe<Array<EventScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<EventOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<EventWhereInput>;
};

export type QueryExpenseArgs = {
  where: ExpenseWhereUniqueInput;
};

export type QueryExpensesArgs = {
  cursor?: InputMaybe<ExpenseWhereUniqueInput>;
  distinct?: InputMaybe<Array<ExpenseScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<ExpenseOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ExpenseWhereInput>;
};

export type QueryFindFirstEventArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  distinct?: InputMaybe<Array<EventScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<EventOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<EventWhereInput>;
};

export type QueryFindFirstEventOrThrowArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  distinct?: InputMaybe<Array<EventScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<EventOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<EventWhereInput>;
};

export type QueryFindFirstExpenseArgs = {
  cursor?: InputMaybe<ExpenseWhereUniqueInput>;
  distinct?: InputMaybe<Array<ExpenseScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<ExpenseOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ExpenseWhereInput>;
};

export type QueryFindFirstExpenseOrThrowArgs = {
  cursor?: InputMaybe<ExpenseWhereUniqueInput>;
  distinct?: InputMaybe<Array<ExpenseScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<ExpenseOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ExpenseWhereInput>;
};

export type QueryFindFirstUserArgs = {
  cursor?: InputMaybe<UserWhereUniqueInput>;
  distinct?: InputMaybe<Array<UserScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<UserOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserWhereInput>;
};

export type QueryFindFirstUserAttendaceArgs = {
  cursor?: InputMaybe<UserAttendaceWhereUniqueInput>;
  distinct?: InputMaybe<Array<UserAttendaceScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<UserAttendaceOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type QueryFindFirstUserAttendaceOrThrowArgs = {
  cursor?: InputMaybe<UserAttendaceWhereUniqueInput>;
  distinct?: InputMaybe<Array<UserAttendaceScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<UserAttendaceOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type QueryFindFirstUserOrThrowArgs = {
  cursor?: InputMaybe<UserWhereUniqueInput>;
  distinct?: InputMaybe<Array<UserScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<UserOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserWhereInput>;
};

export type QueryFindFirstWalletArgs = {
  cursor?: InputMaybe<WalletWhereUniqueInput>;
  distinct?: InputMaybe<Array<WalletScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<WalletOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<WalletWhereInput>;
};

export type QueryFindFirstWalletOrThrowArgs = {
  cursor?: InputMaybe<WalletWhereUniqueInput>;
  distinct?: InputMaybe<Array<WalletScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<WalletOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<WalletWhereInput>;
};

export type QueryGetEventArgs = {
  where: EventWhereUniqueInput;
};

export type QueryGetExpenseArgs = {
  where: ExpenseWhereUniqueInput;
};

export type QueryGetUserArgs = {
  where: UserWhereUniqueInput;
};

export type QueryGetUserAttendaceArgs = {
  where: UserAttendaceWhereUniqueInput;
};

export type QueryGetWalletArgs = {
  where: WalletWhereUniqueInput;
};

export type QueryGroupByEventArgs = {
  by: Array<EventScalarFieldEnum>;
  having?: InputMaybe<EventScalarWhereWithAggregatesInput>;
  orderBy?: InputMaybe<Array<EventOrderByWithAggregationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<EventWhereInput>;
};

export type QueryGroupByExpenseArgs = {
  by: Array<ExpenseScalarFieldEnum>;
  having?: InputMaybe<ExpenseScalarWhereWithAggregatesInput>;
  orderBy?: InputMaybe<Array<ExpenseOrderByWithAggregationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ExpenseWhereInput>;
};

export type QueryGroupByUserArgs = {
  by: Array<UserScalarFieldEnum>;
  having?: InputMaybe<UserScalarWhereWithAggregatesInput>;
  orderBy?: InputMaybe<Array<UserOrderByWithAggregationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserWhereInput>;
};

export type QueryGroupByUserAttendaceArgs = {
  by: Array<UserAttendaceScalarFieldEnum>;
  having?: InputMaybe<UserAttendaceScalarWhereWithAggregatesInput>;
  orderBy?: InputMaybe<Array<UserAttendaceOrderByWithAggregationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type QueryGroupByWalletArgs = {
  by: Array<WalletScalarFieldEnum>;
  having?: InputMaybe<WalletScalarWhereWithAggregatesInput>;
  orderBy?: InputMaybe<Array<WalletOrderByWithAggregationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<WalletWhereInput>;
};

export type QueryUserArgs = {
  where: UserWhereUniqueInput;
};

export type QueryUserAttendaceArgs = {
  where: UserAttendaceWhereUniqueInput;
};

export type QueryUserAttendacesArgs = {
  cursor?: InputMaybe<UserAttendaceWhereUniqueInput>;
  distinct?: InputMaybe<Array<UserAttendaceScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<UserAttendaceOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type QueryUsersArgs = {
  cursor?: InputMaybe<UserWhereUniqueInput>;
  distinct?: InputMaybe<Array<UserScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<UserOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserWhereInput>;
};

export type QueryWalletArgs = {
  where: WalletWhereUniqueInput;
};

export type QueryWalletsArgs = {
  cursor?: InputMaybe<WalletWhereUniqueInput>;
  distinct?: InputMaybe<Array<WalletScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<WalletOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<WalletWhereInput>;
};

export enum QueryMode {
  Default = 'default',
  Insensitive = 'insensitive',
}

export enum SortOrder {
  Asc = 'asc',
  Desc = 'desc',
}

export type SortOrderInput = {
  nulls?: InputMaybe<NullsOrder>;
  sort: SortOrder;
};

export type StringFieldUpdateOperationsInput = {
  set?: InputMaybe<Scalars['String']['input']>;
};

export type StringFilter = {
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type StringNullableFilter = {
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type StringNullableWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntNullableFilter>;
  _max?: InputMaybe<NestedStringNullableFilter>;
  _min?: InputMaybe<NestedStringNullableFilter>;
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringNullableWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type StringWithAggregatesFilter = {
  _count?: InputMaybe<NestedIntFilter>;
  _max?: InputMaybe<NestedStringFilter>;
  _min?: InputMaybe<NestedStringFilter>;
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  equals?: InputMaybe<Scalars['String']['input']>;
  gt?: InputMaybe<Scalars['String']['input']>;
  gte?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  lt?: InputMaybe<Scalars['String']['input']>;
  lte?: InputMaybe<Scalars['String']['input']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringWithAggregatesFilter>;
  notIn?: InputMaybe<Array<Scalars['String']['input']>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type User = {
  __typename?: 'User';
  _count?: Maybe<UserCount>;
  attendaces: Array<UserAttendace>;
  email?: Maybe<Scalars['String']['output']>;
  firstName?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  isAdmin: Scalars['Boolean']['output'];
  lastName?: Maybe<Scalars['String']['output']>;
  wallets: Array<Wallet>;
};

export type UserAttendacesArgs = {
  cursor?: InputMaybe<UserAttendaceWhereUniqueInput>;
  distinct?: InputMaybe<Array<UserAttendaceScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<UserAttendaceOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type UserWalletsArgs = {
  cursor?: InputMaybe<WalletWhereUniqueInput>;
  distinct?: InputMaybe<Array<WalletScalarFieldEnum>>;
  orderBy?: InputMaybe<Array<WalletOrderByWithRelationInput>>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<WalletWhereInput>;
};

export type UserAttendace = {
  __typename?: 'UserAttendace';
  amount?: Maybe<Scalars['Float']['output']>;
  event: Event;
  eventId: Scalars['String']['output'];
  id: Scalars['String']['output'];
  user: User;
  userId: Scalars['String']['output'];
};

export type UserAttendaceAvgAggregate = {
  __typename?: 'UserAttendaceAvgAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
};

export type UserAttendaceAvgOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
};

export type UserAttendaceCountAggregate = {
  __typename?: 'UserAttendaceCountAggregate';
  _all: Scalars['Int']['output'];
  amount: Scalars['Int']['output'];
  eventId: Scalars['Int']['output'];
  id: Scalars['Int']['output'];
  userId: Scalars['Int']['output'];
};

export type UserAttendaceCountOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  eventId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type UserAttendaceCreateInput = {
  amount?: InputMaybe<Scalars['Float']['input']>;
  event: EventCreateNestedOneWithoutUsersInput;
  id?: InputMaybe<Scalars['String']['input']>;
  user: UserCreateNestedOneWithoutAttendacesInput;
};

export type UserAttendaceCreateManyEventInput = {
  amount?: InputMaybe<Scalars['Float']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  userId: Scalars['String']['input'];
};

export type UserAttendaceCreateManyEventInputEnvelope = {
  data: Array<UserAttendaceCreateManyEventInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']['input']>;
};

export type UserAttendaceCreateManyInput = {
  amount?: InputMaybe<Scalars['Float']['input']>;
  eventId: Scalars['String']['input'];
  id?: InputMaybe<Scalars['String']['input']>;
  userId: Scalars['String']['input'];
};

export type UserAttendaceCreateManyUserInput = {
  amount?: InputMaybe<Scalars['Float']['input']>;
  eventId: Scalars['String']['input'];
  id?: InputMaybe<Scalars['String']['input']>;
};

export type UserAttendaceCreateManyUserInputEnvelope = {
  data: Array<UserAttendaceCreateManyUserInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']['input']>;
};

export type UserAttendaceCreateNestedManyWithoutEventInput = {
  connect?: InputMaybe<Array<UserAttendaceWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<
    Array<UserAttendaceCreateOrConnectWithoutEventInput>
  >;
  create?: InputMaybe<Array<UserAttendaceCreateWithoutEventInput>>;
  createMany?: InputMaybe<UserAttendaceCreateManyEventInputEnvelope>;
};

export type UserAttendaceCreateNestedManyWithoutUserInput = {
  connect?: InputMaybe<Array<UserAttendaceWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<
    Array<UserAttendaceCreateOrConnectWithoutUserInput>
  >;
  create?: InputMaybe<Array<UserAttendaceCreateWithoutUserInput>>;
  createMany?: InputMaybe<UserAttendaceCreateManyUserInputEnvelope>;
};

export type UserAttendaceCreateOrConnectWithoutEventInput = {
  create: UserAttendaceCreateWithoutEventInput;
  where: UserAttendaceWhereUniqueInput;
};

export type UserAttendaceCreateOrConnectWithoutUserInput = {
  create: UserAttendaceCreateWithoutUserInput;
  where: UserAttendaceWhereUniqueInput;
};

export type UserAttendaceCreateWithoutEventInput = {
  amount?: InputMaybe<Scalars['Float']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  user: UserCreateNestedOneWithoutAttendacesInput;
};

export type UserAttendaceCreateWithoutUserInput = {
  amount?: InputMaybe<Scalars['Float']['input']>;
  event: EventCreateNestedOneWithoutUsersInput;
  id?: InputMaybe<Scalars['String']['input']>;
};

export type UserAttendaceGroupBy = {
  __typename?: 'UserAttendaceGroupBy';
  _avg?: Maybe<UserAttendaceAvgAggregate>;
  _count?: Maybe<UserAttendaceCountAggregate>;
  _max?: Maybe<UserAttendaceMaxAggregate>;
  _min?: Maybe<UserAttendaceMinAggregate>;
  _sum?: Maybe<UserAttendaceSumAggregate>;
  amount?: Maybe<Scalars['Float']['output']>;
  eventId: Scalars['String']['output'];
  id: Scalars['String']['output'];
  userId: Scalars['String']['output'];
};

export type UserAttendaceListRelationFilter = {
  every?: InputMaybe<UserAttendaceWhereInput>;
  none?: InputMaybe<UserAttendaceWhereInput>;
  some?: InputMaybe<UserAttendaceWhereInput>;
};

export type UserAttendaceMaxAggregate = {
  __typename?: 'UserAttendaceMaxAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  userId?: Maybe<Scalars['String']['output']>;
};

export type UserAttendaceMaxOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  eventId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type UserAttendaceMinAggregate = {
  __typename?: 'UserAttendaceMinAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
  eventId?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  userId?: Maybe<Scalars['String']['output']>;
};

export type UserAttendaceMinOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  eventId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type UserAttendaceOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type UserAttendaceOrderByWithAggregationInput = {
  _avg?: InputMaybe<UserAttendaceAvgOrderByAggregateInput>;
  _count?: InputMaybe<UserAttendaceCountOrderByAggregateInput>;
  _max?: InputMaybe<UserAttendaceMaxOrderByAggregateInput>;
  _min?: InputMaybe<UserAttendaceMinOrderByAggregateInput>;
  _sum?: InputMaybe<UserAttendaceSumOrderByAggregateInput>;
  amount?: InputMaybe<SortOrderInput>;
  eventId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type UserAttendaceOrderByWithRelationInput = {
  amount?: InputMaybe<SortOrderInput>;
  event?: InputMaybe<EventOrderByWithRelationInput>;
  eventId?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  user?: InputMaybe<UserOrderByWithRelationInput>;
  userId?: InputMaybe<SortOrder>;
};

export enum UserAttendaceScalarFieldEnum {
  Amount = 'amount',
  EventId = 'eventId',
  Id = 'id',
  UserId = 'userId',
}

export type UserAttendaceScalarWhereInput = {
  AND?: InputMaybe<Array<UserAttendaceScalarWhereInput>>;
  NOT?: InputMaybe<Array<UserAttendaceScalarWhereInput>>;
  OR?: InputMaybe<Array<UserAttendaceScalarWhereInput>>;
  amount?: InputMaybe<FloatNullableFilter>;
  eventId?: InputMaybe<StringFilter>;
  id?: InputMaybe<StringFilter>;
  userId?: InputMaybe<StringFilter>;
};

export type UserAttendaceScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<UserAttendaceScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<UserAttendaceScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<UserAttendaceScalarWhereWithAggregatesInput>>;
  amount?: InputMaybe<FloatNullableWithAggregatesFilter>;
  eventId?: InputMaybe<StringWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  userId?: InputMaybe<StringWithAggregatesFilter>;
};

export type UserAttendaceSumAggregate = {
  __typename?: 'UserAttendaceSumAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
};

export type UserAttendaceSumOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
};

export type UserAttendaceUpdateInput = {
  amount?: InputMaybe<NullableFloatFieldUpdateOperationsInput>;
  event?: InputMaybe<EventUpdateOneRequiredWithoutUsersNestedInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  user?: InputMaybe<UserUpdateOneRequiredWithoutAttendacesNestedInput>;
};

export type UserAttendaceUpdateManyMutationInput = {
  amount?: InputMaybe<NullableFloatFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type UserAttendaceUpdateManyWithWhereWithoutEventInput = {
  data: UserAttendaceUpdateManyMutationInput;
  where: UserAttendaceScalarWhereInput;
};

export type UserAttendaceUpdateManyWithWhereWithoutUserInput = {
  data: UserAttendaceUpdateManyMutationInput;
  where: UserAttendaceScalarWhereInput;
};

export type UserAttendaceUpdateManyWithoutEventNestedInput = {
  connect?: InputMaybe<Array<UserAttendaceWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<
    Array<UserAttendaceCreateOrConnectWithoutEventInput>
  >;
  create?: InputMaybe<Array<UserAttendaceCreateWithoutEventInput>>;
  createMany?: InputMaybe<UserAttendaceCreateManyEventInputEnvelope>;
  delete?: InputMaybe<Array<UserAttendaceWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<UserAttendaceScalarWhereInput>>;
  disconnect?: InputMaybe<Array<UserAttendaceWhereUniqueInput>>;
  set?: InputMaybe<Array<UserAttendaceWhereUniqueInput>>;
  update?: InputMaybe<
    Array<UserAttendaceUpdateWithWhereUniqueWithoutEventInput>
  >;
  updateMany?: InputMaybe<
    Array<UserAttendaceUpdateManyWithWhereWithoutEventInput>
  >;
  upsert?: InputMaybe<
    Array<UserAttendaceUpsertWithWhereUniqueWithoutEventInput>
  >;
};

export type UserAttendaceUpdateManyWithoutUserNestedInput = {
  connect?: InputMaybe<Array<UserAttendaceWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<
    Array<UserAttendaceCreateOrConnectWithoutUserInput>
  >;
  create?: InputMaybe<Array<UserAttendaceCreateWithoutUserInput>>;
  createMany?: InputMaybe<UserAttendaceCreateManyUserInputEnvelope>;
  delete?: InputMaybe<Array<UserAttendaceWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<UserAttendaceScalarWhereInput>>;
  disconnect?: InputMaybe<Array<UserAttendaceWhereUniqueInput>>;
  set?: InputMaybe<Array<UserAttendaceWhereUniqueInput>>;
  update?: InputMaybe<
    Array<UserAttendaceUpdateWithWhereUniqueWithoutUserInput>
  >;
  updateMany?: InputMaybe<
    Array<UserAttendaceUpdateManyWithWhereWithoutUserInput>
  >;
  upsert?: InputMaybe<
    Array<UserAttendaceUpsertWithWhereUniqueWithoutUserInput>
  >;
};

export type UserAttendaceUpdateWithWhereUniqueWithoutEventInput = {
  data: UserAttendaceUpdateWithoutEventInput;
  where: UserAttendaceWhereUniqueInput;
};

export type UserAttendaceUpdateWithWhereUniqueWithoutUserInput = {
  data: UserAttendaceUpdateWithoutUserInput;
  where: UserAttendaceWhereUniqueInput;
};

export type UserAttendaceUpdateWithoutEventInput = {
  amount?: InputMaybe<NullableFloatFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  user?: InputMaybe<UserUpdateOneRequiredWithoutAttendacesNestedInput>;
};

export type UserAttendaceUpdateWithoutUserInput = {
  amount?: InputMaybe<NullableFloatFieldUpdateOperationsInput>;
  event?: InputMaybe<EventUpdateOneRequiredWithoutUsersNestedInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type UserAttendaceUpsertWithWhereUniqueWithoutEventInput = {
  create: UserAttendaceCreateWithoutEventInput;
  update: UserAttendaceUpdateWithoutEventInput;
  where: UserAttendaceWhereUniqueInput;
};

export type UserAttendaceUpsertWithWhereUniqueWithoutUserInput = {
  create: UserAttendaceCreateWithoutUserInput;
  update: UserAttendaceUpdateWithoutUserInput;
  where: UserAttendaceWhereUniqueInput;
};

export type UserAttendaceWhereInput = {
  AND?: InputMaybe<Array<UserAttendaceWhereInput>>;
  NOT?: InputMaybe<Array<UserAttendaceWhereInput>>;
  OR?: InputMaybe<Array<UserAttendaceWhereInput>>;
  amount?: InputMaybe<FloatNullableFilter>;
  event?: InputMaybe<EventRelationFilter>;
  eventId?: InputMaybe<StringFilter>;
  id?: InputMaybe<StringFilter>;
  user?: InputMaybe<UserRelationFilter>;
  userId?: InputMaybe<StringFilter>;
};

export type UserAttendaceWhereUniqueInput = {
  AND?: InputMaybe<Array<UserAttendaceWhereInput>>;
  NOT?: InputMaybe<Array<UserAttendaceWhereInput>>;
  OR?: InputMaybe<Array<UserAttendaceWhereInput>>;
  amount?: InputMaybe<FloatNullableFilter>;
  event?: InputMaybe<EventRelationFilter>;
  eventId?: InputMaybe<StringFilter>;
  id?: InputMaybe<Scalars['String']['input']>;
  user?: InputMaybe<UserRelationFilter>;
  userId?: InputMaybe<StringFilter>;
};

export type UserCount = {
  __typename?: 'UserCount';
  attendaces: Scalars['Int']['output'];
  wallets: Scalars['Int']['output'];
};

export type UserCountAttendacesArgs = {
  where?: InputMaybe<UserAttendaceWhereInput>;
};

export type UserCountWalletsArgs = {
  where?: InputMaybe<WalletWhereInput>;
};

export type UserCountAggregate = {
  __typename?: 'UserCountAggregate';
  _all: Scalars['Int']['output'];
  email: Scalars['Int']['output'];
  firstName: Scalars['Int']['output'];
  id: Scalars['Int']['output'];
  isAdmin: Scalars['Int']['output'];
  lastName: Scalars['Int']['output'];
  password: Scalars['Int']['output'];
  tokenSalt: Scalars['Int']['output'];
};

export type UserCountOrderByAggregateInput = {
  email?: InputMaybe<SortOrder>;
  firstName?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isAdmin?: InputMaybe<SortOrder>;
  lastName?: InputMaybe<SortOrder>;
  password?: InputMaybe<SortOrder>;
  tokenSalt?: InputMaybe<SortOrder>;
};

export type UserCreateInput = {
  attendaces?: InputMaybe<UserAttendaceCreateNestedManyWithoutUserInput>;
  email?: InputMaybe<Scalars['String']['input']>;
  firstName?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  isAdmin?: InputMaybe<Scalars['Boolean']['input']>;
  lastName?: InputMaybe<Scalars['String']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
  tokenSalt?: InputMaybe<Scalars['String']['input']>;
  wallets?: InputMaybe<WalletCreateNestedManyWithoutUserInput>;
};

export type UserCreateManyInput = {
  email?: InputMaybe<Scalars['String']['input']>;
  firstName?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  isAdmin?: InputMaybe<Scalars['Boolean']['input']>;
  lastName?: InputMaybe<Scalars['String']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
  tokenSalt?: InputMaybe<Scalars['String']['input']>;
};

export type UserCreateNestedOneWithoutAttendacesInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutAttendacesInput>;
  create?: InputMaybe<UserCreateWithoutAttendacesInput>;
};

export type UserCreateNestedOneWithoutWalletsInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutWalletsInput>;
  create?: InputMaybe<UserCreateWithoutWalletsInput>;
};

export type UserCreateOrConnectWithoutAttendacesInput = {
  create: UserCreateWithoutAttendacesInput;
  where: UserWhereUniqueInput;
};

export type UserCreateOrConnectWithoutWalletsInput = {
  create: UserCreateWithoutWalletsInput;
  where: UserWhereUniqueInput;
};

export type UserCreateWithoutAttendacesInput = {
  email?: InputMaybe<Scalars['String']['input']>;
  firstName?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  isAdmin?: InputMaybe<Scalars['Boolean']['input']>;
  lastName?: InputMaybe<Scalars['String']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
  tokenSalt?: InputMaybe<Scalars['String']['input']>;
  wallets?: InputMaybe<WalletCreateNestedManyWithoutUserInput>;
};

export type UserCreateWithoutWalletsInput = {
  attendaces?: InputMaybe<UserAttendaceCreateNestedManyWithoutUserInput>;
  email?: InputMaybe<Scalars['String']['input']>;
  firstName?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  isAdmin?: InputMaybe<Scalars['Boolean']['input']>;
  lastName?: InputMaybe<Scalars['String']['input']>;
  password?: InputMaybe<Scalars['String']['input']>;
  tokenSalt?: InputMaybe<Scalars['String']['input']>;
};

export type UserGroupBy = {
  __typename?: 'UserGroupBy';
  _count?: Maybe<UserCountAggregate>;
  _max?: Maybe<UserMaxAggregate>;
  _min?: Maybe<UserMinAggregate>;
  email?: Maybe<Scalars['String']['output']>;
  firstName?: Maybe<Scalars['String']['output']>;
  id: Scalars['String']['output'];
  isAdmin: Scalars['Boolean']['output'];
  lastName?: Maybe<Scalars['String']['output']>;
  password?: Maybe<Scalars['String']['output']>;
  tokenSalt: Scalars['String']['output'];
};

export type UserMaxAggregate = {
  __typename?: 'UserMaxAggregate';
  email?: Maybe<Scalars['String']['output']>;
  firstName?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  isAdmin?: Maybe<Scalars['Boolean']['output']>;
  lastName?: Maybe<Scalars['String']['output']>;
  password?: Maybe<Scalars['String']['output']>;
  tokenSalt?: Maybe<Scalars['String']['output']>;
};

export type UserMaxOrderByAggregateInput = {
  email?: InputMaybe<SortOrder>;
  firstName?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isAdmin?: InputMaybe<SortOrder>;
  lastName?: InputMaybe<SortOrder>;
  password?: InputMaybe<SortOrder>;
  tokenSalt?: InputMaybe<SortOrder>;
};

export type UserMinAggregate = {
  __typename?: 'UserMinAggregate';
  email?: Maybe<Scalars['String']['output']>;
  firstName?: Maybe<Scalars['String']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  isAdmin?: Maybe<Scalars['Boolean']['output']>;
  lastName?: Maybe<Scalars['String']['output']>;
  password?: Maybe<Scalars['String']['output']>;
  tokenSalt?: Maybe<Scalars['String']['output']>;
};

export type UserMinOrderByAggregateInput = {
  email?: InputMaybe<SortOrder>;
  firstName?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  isAdmin?: InputMaybe<SortOrder>;
  lastName?: InputMaybe<SortOrder>;
  password?: InputMaybe<SortOrder>;
  tokenSalt?: InputMaybe<SortOrder>;
};

export type UserOrderByWithAggregationInput = {
  _count?: InputMaybe<UserCountOrderByAggregateInput>;
  _max?: InputMaybe<UserMaxOrderByAggregateInput>;
  _min?: InputMaybe<UserMinOrderByAggregateInput>;
  email?: InputMaybe<SortOrderInput>;
  firstName?: InputMaybe<SortOrderInput>;
  id?: InputMaybe<SortOrder>;
  isAdmin?: InputMaybe<SortOrder>;
  lastName?: InputMaybe<SortOrderInput>;
  password?: InputMaybe<SortOrderInput>;
  tokenSalt?: InputMaybe<SortOrder>;
};

export type UserOrderByWithRelationInput = {
  attendaces?: InputMaybe<UserAttendaceOrderByRelationAggregateInput>;
  email?: InputMaybe<SortOrderInput>;
  firstName?: InputMaybe<SortOrderInput>;
  id?: InputMaybe<SortOrder>;
  isAdmin?: InputMaybe<SortOrder>;
  lastName?: InputMaybe<SortOrderInput>;
  password?: InputMaybe<SortOrderInput>;
  tokenSalt?: InputMaybe<SortOrder>;
  wallets?: InputMaybe<WalletOrderByRelationAggregateInput>;
};

export type UserRelationFilter = {
  is?: InputMaybe<UserWhereInput>;
  isNot?: InputMaybe<UserWhereInput>;
};

export enum UserScalarFieldEnum {
  Email = 'email',
  FirstName = 'firstName',
  Id = 'id',
  IsAdmin = 'isAdmin',
  LastName = 'lastName',
  Password = 'password',
  TokenSalt = 'tokenSalt',
}

export type UserScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<UserScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<UserScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<UserScalarWhereWithAggregatesInput>>;
  email?: InputMaybe<StringNullableWithAggregatesFilter>;
  firstName?: InputMaybe<StringNullableWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  isAdmin?: InputMaybe<BoolWithAggregatesFilter>;
  lastName?: InputMaybe<StringNullableWithAggregatesFilter>;
  password?: InputMaybe<StringNullableWithAggregatesFilter>;
  tokenSalt?: InputMaybe<StringWithAggregatesFilter>;
};

export type UserUpdateInput = {
  attendaces?: InputMaybe<UserAttendaceUpdateManyWithoutUserNestedInput>;
  email?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  firstName?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  lastName?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  password?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  tokenSalt?: InputMaybe<StringFieldUpdateOperationsInput>;
  wallets?: InputMaybe<WalletUpdateManyWithoutUserNestedInput>;
};

export type UserUpdateManyMutationInput = {
  email?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  firstName?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  lastName?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  password?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  tokenSalt?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type UserUpdateOneRequiredWithoutAttendacesNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutAttendacesInput>;
  create?: InputMaybe<UserCreateWithoutAttendacesInput>;
  update?: InputMaybe<UserUpdateToOneWithWhereWithoutAttendacesInput>;
  upsert?: InputMaybe<UserUpsertWithoutAttendacesInput>;
};

export type UserUpdateOneRequiredWithoutWalletsNestedInput = {
  connect?: InputMaybe<UserWhereUniqueInput>;
  connectOrCreate?: InputMaybe<UserCreateOrConnectWithoutWalletsInput>;
  create?: InputMaybe<UserCreateWithoutWalletsInput>;
  update?: InputMaybe<UserUpdateToOneWithWhereWithoutWalletsInput>;
  upsert?: InputMaybe<UserUpsertWithoutWalletsInput>;
};

export type UserUpdateToOneWithWhereWithoutAttendacesInput = {
  data: UserUpdateWithoutAttendacesInput;
  where?: InputMaybe<UserWhereInput>;
};

export type UserUpdateToOneWithWhereWithoutWalletsInput = {
  data: UserUpdateWithoutWalletsInput;
  where?: InputMaybe<UserWhereInput>;
};

export type UserUpdateWithoutAttendacesInput = {
  email?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  firstName?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  lastName?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  password?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  tokenSalt?: InputMaybe<StringFieldUpdateOperationsInput>;
  wallets?: InputMaybe<WalletUpdateManyWithoutUserNestedInput>;
};

export type UserUpdateWithoutWalletsInput = {
  attendaces?: InputMaybe<UserAttendaceUpdateManyWithoutUserNestedInput>;
  email?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  firstName?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  isAdmin?: InputMaybe<BoolFieldUpdateOperationsInput>;
  lastName?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  password?: InputMaybe<NullableStringFieldUpdateOperationsInput>;
  tokenSalt?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type UserUpsertWithoutAttendacesInput = {
  create: UserCreateWithoutAttendacesInput;
  update: UserUpdateWithoutAttendacesInput;
  where?: InputMaybe<UserWhereInput>;
};

export type UserUpsertWithoutWalletsInput = {
  create: UserCreateWithoutWalletsInput;
  update: UserUpdateWithoutWalletsInput;
  where?: InputMaybe<UserWhereInput>;
};

export type UserWhereInput = {
  AND?: InputMaybe<Array<UserWhereInput>>;
  NOT?: InputMaybe<Array<UserWhereInput>>;
  OR?: InputMaybe<Array<UserWhereInput>>;
  attendaces?: InputMaybe<UserAttendaceListRelationFilter>;
  email?: InputMaybe<StringNullableFilter>;
  firstName?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<StringFilter>;
  isAdmin?: InputMaybe<BoolFilter>;
  lastName?: InputMaybe<StringNullableFilter>;
  password?: InputMaybe<StringNullableFilter>;
  tokenSalt?: InputMaybe<StringFilter>;
  wallets?: InputMaybe<WalletListRelationFilter>;
};

export type UserWhereUniqueInput = {
  AND?: InputMaybe<Array<UserWhereInput>>;
  NOT?: InputMaybe<Array<UserWhereInput>>;
  OR?: InputMaybe<Array<UserWhereInput>>;
  attendaces?: InputMaybe<UserAttendaceListRelationFilter>;
  email?: InputMaybe<Scalars['String']['input']>;
  firstName?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<Scalars['String']['input']>;
  isAdmin?: InputMaybe<BoolFilter>;
  lastName?: InputMaybe<StringNullableFilter>;
  password?: InputMaybe<StringNullableFilter>;
  tokenSalt?: InputMaybe<StringFilter>;
  wallets?: InputMaybe<WalletListRelationFilter>;
};

export type Wallet = {
  __typename?: 'Wallet';
  amount: Scalars['Float']['output'];
  createdAt: Scalars['DateTimeISO']['output'];
  id: Scalars['String']['output'];
  user: User;
  userId: Scalars['String']['output'];
};

export type WalletAvgAggregate = {
  __typename?: 'WalletAvgAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
};

export type WalletAvgOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
};

export type WalletCountAggregate = {
  __typename?: 'WalletCountAggregate';
  _all: Scalars['Int']['output'];
  amount: Scalars['Int']['output'];
  createdAt: Scalars['Int']['output'];
  id: Scalars['Int']['output'];
  userId: Scalars['Int']['output'];
};

export type WalletCountOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type WalletCreateInput = {
  amount: Scalars['Float']['input'];
  createdAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  user: UserCreateNestedOneWithoutWalletsInput;
};

export type WalletCreateManyInput = {
  amount: Scalars['Float']['input'];
  createdAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
  userId: Scalars['String']['input'];
};

export type WalletCreateManyUserInput = {
  amount: Scalars['Float']['input'];
  createdAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
};

export type WalletCreateManyUserInputEnvelope = {
  data: Array<WalletCreateManyUserInput>;
  skipDuplicates?: InputMaybe<Scalars['Boolean']['input']>;
};

export type WalletCreateNestedManyWithoutUserInput = {
  connect?: InputMaybe<Array<WalletWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<WalletCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<Array<WalletCreateWithoutUserInput>>;
  createMany?: InputMaybe<WalletCreateManyUserInputEnvelope>;
};

export type WalletCreateOrConnectWithoutUserInput = {
  create: WalletCreateWithoutUserInput;
  where: WalletWhereUniqueInput;
};

export type WalletCreateWithoutUserInput = {
  amount: Scalars['Float']['input'];
  createdAt?: InputMaybe<Scalars['DateTimeISO']['input']>;
  id?: InputMaybe<Scalars['String']['input']>;
};

export type WalletGroupBy = {
  __typename?: 'WalletGroupBy';
  _avg?: Maybe<WalletAvgAggregate>;
  _count?: Maybe<WalletCountAggregate>;
  _max?: Maybe<WalletMaxAggregate>;
  _min?: Maybe<WalletMinAggregate>;
  _sum?: Maybe<WalletSumAggregate>;
  amount: Scalars['Float']['output'];
  createdAt: Scalars['DateTimeISO']['output'];
  id: Scalars['String']['output'];
  userId: Scalars['String']['output'];
};

export type WalletListRelationFilter = {
  every?: InputMaybe<WalletWhereInput>;
  none?: InputMaybe<WalletWhereInput>;
  some?: InputMaybe<WalletWhereInput>;
};

export type WalletMaxAggregate = {
  __typename?: 'WalletMaxAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
  createdAt?: Maybe<Scalars['DateTimeISO']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  userId?: Maybe<Scalars['String']['output']>;
};

export type WalletMaxOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type WalletMinAggregate = {
  __typename?: 'WalletMinAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
  createdAt?: Maybe<Scalars['DateTimeISO']['output']>;
  id?: Maybe<Scalars['String']['output']>;
  userId?: Maybe<Scalars['String']['output']>;
};

export type WalletMinOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type WalletOrderByRelationAggregateInput = {
  _count?: InputMaybe<SortOrder>;
};

export type WalletOrderByWithAggregationInput = {
  _avg?: InputMaybe<WalletAvgOrderByAggregateInput>;
  _count?: InputMaybe<WalletCountOrderByAggregateInput>;
  _max?: InputMaybe<WalletMaxOrderByAggregateInput>;
  _min?: InputMaybe<WalletMinOrderByAggregateInput>;
  _sum?: InputMaybe<WalletSumOrderByAggregateInput>;
  amount?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  userId?: InputMaybe<SortOrder>;
};

export type WalletOrderByWithRelationInput = {
  amount?: InputMaybe<SortOrder>;
  createdAt?: InputMaybe<SortOrder>;
  id?: InputMaybe<SortOrder>;
  user?: InputMaybe<UserOrderByWithRelationInput>;
  userId?: InputMaybe<SortOrder>;
};

export enum WalletScalarFieldEnum {
  Amount = 'amount',
  CreatedAt = 'createdAt',
  Id = 'id',
  UserId = 'userId',
}

export type WalletScalarWhereInput = {
  AND?: InputMaybe<Array<WalletScalarWhereInput>>;
  NOT?: InputMaybe<Array<WalletScalarWhereInput>>;
  OR?: InputMaybe<Array<WalletScalarWhereInput>>;
  amount?: InputMaybe<FloatFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  userId?: InputMaybe<StringFilter>;
};

export type WalletScalarWhereWithAggregatesInput = {
  AND?: InputMaybe<Array<WalletScalarWhereWithAggregatesInput>>;
  NOT?: InputMaybe<Array<WalletScalarWhereWithAggregatesInput>>;
  OR?: InputMaybe<Array<WalletScalarWhereWithAggregatesInput>>;
  amount?: InputMaybe<FloatWithAggregatesFilter>;
  createdAt?: InputMaybe<DateTimeWithAggregatesFilter>;
  id?: InputMaybe<StringWithAggregatesFilter>;
  userId?: InputMaybe<StringWithAggregatesFilter>;
};

export type WalletSumAggregate = {
  __typename?: 'WalletSumAggregate';
  amount?: Maybe<Scalars['Float']['output']>;
};

export type WalletSumOrderByAggregateInput = {
  amount?: InputMaybe<SortOrder>;
};

export type WalletUpdateInput = {
  amount?: InputMaybe<FloatFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
  user?: InputMaybe<UserUpdateOneRequiredWithoutWalletsNestedInput>;
};

export type WalletUpdateManyMutationInput = {
  amount?: InputMaybe<FloatFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type WalletUpdateManyWithWhereWithoutUserInput = {
  data: WalletUpdateManyMutationInput;
  where: WalletScalarWhereInput;
};

export type WalletUpdateManyWithoutUserNestedInput = {
  connect?: InputMaybe<Array<WalletWhereUniqueInput>>;
  connectOrCreate?: InputMaybe<Array<WalletCreateOrConnectWithoutUserInput>>;
  create?: InputMaybe<Array<WalletCreateWithoutUserInput>>;
  createMany?: InputMaybe<WalletCreateManyUserInputEnvelope>;
  delete?: InputMaybe<Array<WalletWhereUniqueInput>>;
  deleteMany?: InputMaybe<Array<WalletScalarWhereInput>>;
  disconnect?: InputMaybe<Array<WalletWhereUniqueInput>>;
  set?: InputMaybe<Array<WalletWhereUniqueInput>>;
  update?: InputMaybe<Array<WalletUpdateWithWhereUniqueWithoutUserInput>>;
  updateMany?: InputMaybe<Array<WalletUpdateManyWithWhereWithoutUserInput>>;
  upsert?: InputMaybe<Array<WalletUpsertWithWhereUniqueWithoutUserInput>>;
};

export type WalletUpdateWithWhereUniqueWithoutUserInput = {
  data: WalletUpdateWithoutUserInput;
  where: WalletWhereUniqueInput;
};

export type WalletUpdateWithoutUserInput = {
  amount?: InputMaybe<FloatFieldUpdateOperationsInput>;
  createdAt?: InputMaybe<DateTimeFieldUpdateOperationsInput>;
  id?: InputMaybe<StringFieldUpdateOperationsInput>;
};

export type WalletUpsertWithWhereUniqueWithoutUserInput = {
  create: WalletCreateWithoutUserInput;
  update: WalletUpdateWithoutUserInput;
  where: WalletWhereUniqueInput;
};

export type WalletWhereInput = {
  AND?: InputMaybe<Array<WalletWhereInput>>;
  NOT?: InputMaybe<Array<WalletWhereInput>>;
  OR?: InputMaybe<Array<WalletWhereInput>>;
  amount?: InputMaybe<FloatFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<StringFilter>;
  user?: InputMaybe<UserRelationFilter>;
  userId?: InputMaybe<StringFilter>;
};

export type WalletWhereUniqueInput = {
  AND?: InputMaybe<Array<WalletWhereInput>>;
  NOT?: InputMaybe<Array<WalletWhereInput>>;
  OR?: InputMaybe<Array<WalletWhereInput>>;
  amount?: InputMaybe<FloatFilter>;
  createdAt?: InputMaybe<DateTimeFilter>;
  id?: InputMaybe<Scalars['String']['input']>;
  user?: InputMaybe<UserRelationFilter>;
  userId?: InputMaybe<StringFilter>;
};
