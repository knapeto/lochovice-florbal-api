import * as Types from './operations';

import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
const defaultOptions = {} as const;

export const TestMutationDocument = gql`
  mutation testMutation {
    testMutation
  }
`;
export type TestMutationMutationFn = Apollo.MutationFunction<
  Types.TestMutationMutation,
  Types.TestMutationMutationVariables
>;

/**
 * __useTestMutationMutation__
 *
 * To run a mutation, you first call `useTestMutationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useTestMutationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [testMutationMutation, { data, loading, error }] = useTestMutationMutation({
 *   variables: {
 *   },
 * });
 */
export function useTestMutationMutation(
  baseOptions?: Apollo.MutationHookOptions<
    Types.TestMutationMutation,
    Types.TestMutationMutationVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<
    Types.TestMutationMutation,
    Types.TestMutationMutationVariables
  >(TestMutationDocument, options);
}
export type TestMutationMutationHookResult = ReturnType<
  typeof useTestMutationMutation
>;
export type TestMutationMutationResult =
  Apollo.MutationResult<Types.TestMutationMutation>;
export type TestMutationMutationOptions = Apollo.BaseMutationOptions<
  Types.TestMutationMutation,
  Types.TestMutationMutationVariables
>;
export const TestQueryDocument = gql`
  query testQuery {
    testQuery
  }
`;

/**
 * __useTestQueryQuery__
 *
 * To run a query within a React component, call `useTestQueryQuery` and pass it any options that fit your needs.
 * When your component renders, `useTestQueryQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTestQueryQuery({
 *   variables: {
 *   },
 * });
 */
export function useTestQueryQuery(
  baseOptions?: Apollo.QueryHookOptions<
    Types.TestQueryQuery,
    Types.TestQueryQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<Types.TestQueryQuery, Types.TestQueryQueryVariables>(
    TestQueryDocument,
    options,
  );
}
export function useTestQueryLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    Types.TestQueryQuery,
    Types.TestQueryQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<
    Types.TestQueryQuery,
    Types.TestQueryQueryVariables
  >(TestQueryDocument, options);
}
export function useTestQuerySuspenseQuery(
  baseOptions?: Apollo.SuspenseQueryHookOptions<
    Types.TestQueryQuery,
    Types.TestQueryQueryVariables
  >,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSuspenseQuery<
    Types.TestQueryQuery,
    Types.TestQueryQueryVariables
  >(TestQueryDocument, options);
}
export type TestQueryQueryHookResult = ReturnType<typeof useTestQueryQuery>;
export type TestQueryLazyQueryHookResult = ReturnType<
  typeof useTestQueryLazyQuery
>;
export type TestQuerySuspenseQueryHookResult = ReturnType<
  typeof useTestQuerySuspenseQuery
>;
export type TestQueryQueryResult = Apollo.QueryResult<
  Types.TestQueryQuery,
  Types.TestQueryQueryVariables
>;
