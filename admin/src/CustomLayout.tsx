import CustomAppBar from './CustomAppBar';
import { Layout } from 'react-admin';

const CustomLayout = (props) => {
  return <Layout {...props} appBar={CustomAppBar} />;
};

export default CustomLayout;
