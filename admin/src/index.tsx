import './index.css';

import { ApolloProvider } from '@apollo/client';
import App from './App';
import ReactDOM from 'react-dom';
import createApolloClient from './libs/createApolloClient';

export const API = `http://localhost:3000/admin/graphql`;
const client = createApolloClient(API);

ReactDOM.render(
  <ApolloProvider client={client}>
    <App client={client} />
  </ApolloProvider>,
  document.getElementById('root'),
);
