import { I18nProvider } from 'react-admin';
import czechMessages from 'ra-language-czech';
import polyglotI18nProvider from 'ra-i18n-polyglot';

export default polyglotI18nProvider(locale => czechMessages) as I18nProvider;
