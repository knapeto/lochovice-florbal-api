import { FetchType } from "../types";
import { BuildVariablesContext } from "./types";
export interface GetListParams {
    filter: {
        [key: string]: any;
    };
    pagination: {
        page: number;
        perPage: number;
    };
    sort: {
        field: string;
        order: string;
    };
}
export interface UpdateParams {
    id: string;
    data: {
        [key: string]: any;
    };
    previousData: {
        [key: string]: any;
    };
}
export declare const buildVariables: (context: BuildVariablesContext) => (aorFetchType: FetchType, params: any) => {
    skip: number;
    take: number;
    orderBy: {}[];
    where: any;
    aorFetchType: FetchType;
} | {
    data: any;
} | {
    where: {
        id: any;
    };
};
//# sourceMappingURL=index.d.ts.map