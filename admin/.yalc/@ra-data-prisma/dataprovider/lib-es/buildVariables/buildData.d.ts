import { IntrospectionInputObjectType } from "graphql";
import { BuildVariablesContext } from "./types";
export declare type UpdateParams = {
    id: string;
    data: {
        [key: string]: any;
    };
    previousData: {
        [key: string]: any;
    };
};
export declare type CreateParams = {
    data: {
        [key: string]: any;
    };
};
export declare type UpdateManyInput = {
    create?: Array<{
        [key: string]: any;
    }>;
    connect?: Array<{
        id: any;
    }>;
    set?: Array<{
        id: any;
    }>;
    disconnect?: Array<{
        id: any;
    }>;
    delete?: Array<{
        id: any;
    }>;
    update?: Array<{
        where: {
            id: any;
        };
        data: {
            [key: string]: any;
        };
    }>;
};
export declare const buildData: (inputType: IntrospectionInputObjectType, params: UpdateParams | CreateParams, context: BuildVariablesContext) => any;
//# sourceMappingURL=buildData.d.ts.map