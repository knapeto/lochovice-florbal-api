import { FetchType } from "../types";
import { BuildVariablesContext } from "./types";
declare type Filter = {
    NOT?: Filter;
    OR?: Filter[];
    AND?: Filter[];
} & {
    [key: string]: any;
};
export declare const buildWhere: (filter: Filter, context: BuildVariablesContext, aorFetchType: FetchType) => any;
export {};
//# sourceMappingURL=buildWhere.d.ts.map