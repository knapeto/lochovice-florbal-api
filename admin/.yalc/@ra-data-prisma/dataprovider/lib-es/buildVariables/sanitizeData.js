import { has } from "lodash";
/**
 * Due to some implementation details in react-admin, we have to add copies with suffixed keys of certain field data.
 *
 * We transform that back here.
 *
 * this function throws if both variants (suffixed and non suffixed is used) because we cannot decide which one takes precedence
 */
export var getSanitizedFieldData = function (data, previousData, field) {
    var _a;
    var key = field.name;
    var keyWithArraySuffix = key + "_ids";
    console.log(key, data);
    if (has(data, keyWithArraySuffix)) {
        if (has(data, key)) {
            // throw new Error(
            //   `cannot update ${key} and ${keyWithArraySuffix} at the same time. Only use one in the form.`,
            // );
        }
        return {
            fieldData: data[keyWithArraySuffix].map(function (id) { return ({ id: id }); }),
            previousFieldData: previousData
                ? (_a = previousData[keyWithArraySuffix]) === null || _a === void 0 ? void 0 : _a.map(function (id) { return ({ id: id }); })
                : null,
        };
    }
    var keyWithIdsSuffix = key + "_id";
    if (has(data, keyWithIdsSuffix)) {
        if (has(data, key)) {
            // throw new Error(
            //   `cannot update ${key} and ${keyWithIdsSuffix} at the same time. Only use one in the form.`,
            // );
        }
        return {
            fieldData: {
                id: data[keyWithIdsSuffix],
            },
            previousFieldData: previousData
                ? {
                    id: previousData[keyWithIdsSuffix],
                }
                : null,
        };
    }
    return {
        fieldData: data[key],
        previousFieldData: previousData ? previousData[key] : null,
    };
};
export var hasFieldData = function (data, field) {
    return (has(data, field.name) ||
        has(data, field.name + "_id") ||
        has(data, field.name + "_ids"));
};
//# sourceMappingURL=sanitizeData.js.map