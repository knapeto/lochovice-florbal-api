var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import isNil from "lodash/isNil";
import isObject from "lodash/isObject";
import { CREATE, DELETE, GET_LIST, GET_MANY, GET_MANY_REFERENCE, GET_ONE, UPDATE, } from "react-admin";
import { buildWhere } from "./buildWhere";
import { buildData } from "./buildData";
import { buildOrderBy } from "./buildOrderBy";
var buildGetListVariables = function (context) {
    return function (aorFetchType, params) {
        var where = buildWhere(params.filter, context, aorFetchType);
        return {
            skip: (params.pagination.page - 1) * params.pagination.perPage,
            take: params.pagination.perPage,
            orderBy: buildOrderBy(params === null || params === void 0 ? void 0 : params.sort, context),
            where: where,
            aorFetchType: aorFetchType,
        };
    };
};
var getId = function (context, params) {
    var _a, _b;
    var id = (_a = params.id) !== null && _a !== void 0 ? _a : (_b = params.data) === null || _b === void 0 ? void 0 : _b.id;
    var type = context.introspectionResults.types.find(function (t) { return t.name === context.resource.type.name + "WhereUniqueInput"; });
    if (!type || type.kind !== "INPUT_OBJECT")
        return id;
    var idType = type.inputFields.find(function (f) { return f.name === "id"; });
    if (!idType || idType.type.kind !== "SCALAR")
        return id;
    if (idType.type.name === "String") {
        id = String(id);
    }
    if (idType.type.name === "Int") {
        id = parseInt(String(id));
    }
    // should usually not happen
    return id;
};
var buildGetOneVariables = function (context) { return function (params) {
    return {
        where: {
            id: getId(context, params),
        },
    };
}; };
var buildUpdateVariables = function (context) {
    return function (params, parentResource) {
        var _a, _b, _c, _d, _e, _f;
        var inputType = context.introspectionResults.types.find(function (t) { return t.name === context.resource.type.name + "UpdateInput"; });
        var id = getId(context, params); // TODO: do we still need params.data.id?
        delete params.data.id;
        (_a = params.previousData) === null || _a === void 0 ? true : delete _a.id;
        var data = buildData(inputType, params, context);
        return {
            where: {
                id: id,
            },
            data: (_f = (_e = (_d = (_c = (_b = context.options) === null || _b === void 0 ? void 0 : _b.customizeInputData) === null || _c === void 0 ? void 0 : _c[context.resource.type.name]) === null || _d === void 0 ? void 0 : _d.update) === null || _e === void 0 ? void 0 : _e.call(_d, data, params.data)) !== null && _f !== void 0 ? _f : data,
        };
    };
};
var buildCreateVariables = function (context) {
    return function (params, parentResource) {
        var _a, _b, _c, _d, _e;
        var inputType = context.introspectionResults.types.find(function (t) { return t.name === context.resource.type.name + "CreateInput"; });
        var data = buildData(inputType, params, context);
        return {
            data: (_e = (_d = (_c = (_b = (_a = context.options) === null || _a === void 0 ? void 0 : _a.customizeInputData) === null || _b === void 0 ? void 0 : _b[context.resource.type.name]) === null || _c === void 0 ? void 0 : _c.create) === null || _d === void 0 ? void 0 : _d.call(_c, data, params.data)) !== null && _e !== void 0 ? _e : data,
        };
    };
};
export var buildVariables = function (context) {
    return function (aorFetchType, params) {
        var _a;
        switch (aorFetchType) {
            case GET_LIST: {
                return buildGetListVariables(context)(aorFetchType, params);
            }
            case GET_MANY:
                return {
                    where: {
                        id: {
                            in: params.ids
                                .map(function (obj) { return (isObject(obj) ? obj.id : obj); })
                                .filter(function (v) { return !isNil(v); }),
                        },
                    },
                };
            case GET_MANY_REFERENCE: {
                return buildGetListVariables(context)(GET_LIST, __assign(__assign({}, params), { filter: (_a = {}, _a[params.target] = params.id, _a) }));
            }
            case GET_ONE: {
                return buildGetOneVariables(context)(params);
            }
            case UPDATE: {
                return buildUpdateVariables(context)(params);
            }
            case CREATE: {
                return buildCreateVariables(context)(params);
            }
            case DELETE:
                return {
                    where: { id: params.id },
                };
        }
    };
};
//# sourceMappingURL=index.js.map