var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
import isArray from "lodash/isArray";
import isEmpty from "lodash/isEmpty";
import isObject from "lodash/isObject";
import upperFirst from "lodash/upperFirst";
import { sanitizeKey } from "../utils/sanitizeKey";
var getStringFilter = function (key, value, hasCaseInsensitive, comparator) {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    if (hasCaseInsensitive === void 0) { hasCaseInsensitive = false; }
    if (comparator === void 0) { comparator = "contains"; }
    // hasCaseInsensitive is determined from the introspection results
    // no QueryMode => fall back to original implementation
    if (hasCaseInsensitive) {
        return _a = {},
            _a[key] = (_b = {},
                _b[comparator] = value,
                _b.mode = "insensitive",
                _b),
            _a;
    }
    var OR = [
        (_c = {},
            _c[key] = (_d = {},
                _d[comparator] = value,
                _d),
            _c),
    ];
    var valueLowerCased = value.toLowerCase();
    if (valueLowerCased !== value) {
        OR.push((_e = {},
            _e[key] = (_f = {},
                _f[comparator] = valueLowerCased,
                _f),
            _e));
    }
    var valueUpperFirst = upperFirst(value);
    if (valueUpperFirst !== value) {
        OR.push((_g = {},
            _g[key] = (_h = {},
                _h[comparator] = valueUpperFirst,
                _h),
            _g));
    }
    return { OR: OR };
};
var getIntFilter = function (key, value, comparator) {
    var _a, _b;
    if (comparator === void 0) { comparator = "equals"; }
    var asNumber = parseInt(value, 10);
    if (isNaN(asNumber)) {
        return {};
    }
    return _a = {},
        _a[key] = (_b = {},
            _b[comparator] = asNumber,
            _b),
        _a;
};
var getFloatFilter = function (key, value, comparator) {
    var _a, _b;
    if (comparator === void 0) { comparator = "equals"; }
    var asFloat = parseFloat(value);
    if (isNaN(asFloat)) {
        return {};
    }
    return _a = {},
        _a[key] = (_b = {},
            _b[comparator] = asFloat,
            _b),
        _a;
};
var getEnumFilter = function (key, value, comparator) {
    var _a, _b;
    if (comparator === void 0) { comparator = "equals"; }
    return (_a = {},
        _a[key] = (_b = {},
            _b[comparator] = value,
            _b),
        _a);
};
var getBooleanFilter = function (key, value) {
    var _a;
    return _a = {},
        _a[key] = {
            equals: Boolean(value),
        },
        _a;
};
var getDateTimeFilter = function (key, value, comparator) {
    var _a, _b;
    if (comparator === void 0) { comparator = "equals"; }
    return _a = {},
        _a[key] = (_b = {},
            _b[comparator] = new Date(value),
            _b),
        _a;
};
// all these variants have the same fields, and are just used in different places, hence different names
var isDateTimeFilter = function (type) {
    return type.kind === "INPUT_OBJECT" &&
        [
            "DateTimeFilter",
            "DateTimeNullableFilter",
            "NestedDateTimeFilter",
            "NestedDateTimeNullableFilter",
        ].indexOf(type.name) !== -1;
};
var isBooleanFilter = function (type) {
    return type.kind === "INPUT_OBJECT" &&
        [
            "BoolFilter",
            "BoolNullableFilter",
            "NestedBoolFilter",
            "NestedBoolNullableFilter",
        ].indexOf(type.name) !== -1;
};
// StringFilters are a bit special - Nested variants don't have the case insensitive property `mode` on them (otherwise same)
// in case of older Prisma, they're all the same
var isStringFilter = function (type) {
    return type.kind === "INPUT_OBJECT" &&
        ["StringFilter", "StringNullableFilter"].indexOf(type.name) !== -1;
};
var isNestedStringFilter = function (type) {
    return type.kind === "INPUT_OBJECT" &&
        ["NestedStringFilter", "NestedStringNullableFilter"].indexOf(type.name) !==
            -1;
};
var isIntFilter = function (type) {
    return type.kind === "INPUT_OBJECT" &&
        [
            "IntFilter",
            "IntNullableFilter",
            "NestedIntFilter",
            "NestedIntNullableFilter",
        ].indexOf(type.name) !== -1;
};
var isFloatFilter = function (type) {
    return type.kind === "INPUT_OBJECT" &&
        [
            "FloatFilter",
            "FloatNullableFilter",
            "NestedFloatFilter",
            "NestedFloatNullableFilter",
        ].indexOf(type.name) !== -1;
};
var isEnumFilter = function (type) {
    return type.kind === "INPUT_OBJECT" && type.name.startsWith("Enum");
};
var supportsCaseInsensitive = function (introspectionResults) {
    // Prisma 2.8.0 added QueryMode enum (present in StringFilter and StringNullableFilter input objects as property `mode`)
    var queryModeExists = introspectionResults.types.find(function (type) { return type.kind === "ENUM" && type.name === "QueryMode"; });
    var stringFilterType = introspectionResults.types.find(function (type) { return type.kind === "INPUT_OBJECT" && type.name === "StringFilter"; });
    var usesQueryMode = stringFilterType === null || stringFilterType === void 0 ? void 0 : stringFilterType.inputFields.find(function (field) {
        return field.name === "mode" &&
            field.type.kind === "ENUM" &&
            field.type.name === "QueryMode";
    });
    return !!queryModeExists && !!usesQueryMode;
};
var processKey = function (originalKey) {
    // for parsing fields with comparators
    // the original key should be captured in second match unchanged so it shouldn't break anything else
    // equals, contains, startsWith, endsWith are specifically for strings
    // default "comparison" (without specified comparator) is equals, for strings it's contains (so to override this, provide _equals on string fields)
    var keyRegex = /^(.+?)(_(gt|gte|lt|lte|equals|contains|startsWith|endsWith))?$/;
    var matches = originalKey.match(keyRegex);
    var key = matches[1];
    var comparator = matches[3];
    return {
        originalKey: originalKey,
        key: key,
        comparator: comparator,
    };
};
var processComparisonQuery = function (field, value, comparison, whereType, introspectionResults) {
    var _a;
    if (!comparison) {
        return { comparisonPossible: false };
    }
    var comparisonFieldType = (_a = whereType.inputFields.find(function (f) { return f.name === field; })) === null || _a === void 0 ? void 0 : _a.type;
    if (comparisonFieldType) {
        // separated field exists on the whereType
        var filterName_1 = comparisonFieldType.name;
        var filter = introspectionResults.types.find(function (f) { return f.name === filterName_1; });
        var comparatorField = filter.inputFields.find(function (f) { return f.name === comparison; });
        if (comparatorField) {
            // and has the comparison field
            if (!isObject(value) && !isArray(value)) {
                // all comparison fields (equals, lt, lte, gt, gte, contains, startsWith, endsWith) use a Scalar value so it can't be object or array
                return { comparisonPossible: true, comparisonFieldType: filter };
            }
        }
    }
    return { comparisonPossible: false };
};
var getFilters = function (_key, value, whereType, context, aorFetchType) {
    var _a, _b, _c, _d, _e, _f, _g, _h;
    var _j, _k, _l;
    var hasCaseInsensitive = supportsCaseInsensitive(context.introspectionResults);
    var _m = processKey(_key), originalKey = _m.originalKey, key = _m.key, comparator = _m.comparator;
    if ((_j = context.options.filters) === null || _j === void 0 ? void 0 : _j[originalKey]) {
        return (_k = context.options.filters[originalKey](value)) !== null && _k !== void 0 ? _k : {}; // null values are transformed to empty objects
    }
    if (key === "NOT" || key === "OR" || key === "AND") {
        return _a = {},
            _a[key] = value === null
                ? null
                : value.map(function (f) {
                    return buildWhereWithType(f, whereType, context, aorFetchType);
                }),
            _a;
    }
    // check introspection, if we can use comparison on the field before suffixes (and if possible, return the type of that field)
    var _o = processComparisonQuery(key, value, comparator, whereType, context.introspectionResults), comparisonPossible = _o.comparisonPossible, comparisonFieldType = _o.comparisonFieldType;
    if (comparisonPossible) {
        if (isBooleanFilter(comparisonFieldType)) {
            // the only way we could get here is with "boolField_equals" (which is the same as "boolField")
            // but skipping here would lead to trying to find field "boolField_equals" which might not exist
            return getBooleanFilter(key, value);
        }
        if (isStringFilter(comparisonFieldType)) {
            return getStringFilter(key, value, hasCaseInsensitive, comparator);
        }
        // Nested variants don't have `mode` property
        // pass `hasCaseInsensitive=false` to bypass the check and use the original implementation
        if (isNestedStringFilter(comparisonFieldType)) {
            return getStringFilter(key, value, false, comparator);
        }
        if (isDateTimeFilter(comparisonFieldType)) {
            return getDateTimeFilter(key, value, comparator);
        }
        if (isIntFilter(comparisonFieldType)) {
            return getIntFilter(key, value, comparator);
        }
        if (isFloatFilter(comparisonFieldType)) {
            return getFloatFilter(key, value, comparator);
        }
        if (isEnumFilter(comparisonFieldType)) {
            return getEnumFilter(key, value, comparator);
        }
    }
    // we can't use comparison on the splitted field, try to use the original key in a default way (without comparator)
    var fieldType = (_l = whereType.inputFields.find(function (f) { return f.name === originalKey; })) === null || _l === void 0 ? void 0 : _l.type;
    if (!fieldType) {
        if (originalKey === "q") {
            // special: q is universal text search
            // we search all text fields
            // additionaly we split by  space to make a AND connection
            var AND = value.split(" ").map(function (part) { return ({
                OR: whereType.inputFields
                    .map(function (f) {
                    if (f.name !== "id") {
                        if (isStringFilter(f.type)) {
                            return getStringFilter(f.name, part.trim(), hasCaseInsensitive);
                        }
                        if (isNestedStringFilter(f.type)) {
                            return getStringFilter(f.name, part.trim(), false);
                        }
                        if (isIntFilter(f.type)) {
                            var numberValue = parseInt(part.trim());
                            var valueIsInt32 = -(Math.pow(2, 31)) <= numberValue && numberValue <= Math.pow(2, 31) - 1;
                            return valueIsInt32 ? getIntFilter(f.name, part.trim()) : null;
                        }
                    }
                    return null;
                })
                    .filter(function (f) { return !isEmpty(f); }),
            }); });
            return { AND: AND };
        }
        else {
            return {};
        }
    }
    if (!isObject(value) && !isArray(value)) {
        if (isBooleanFilter(fieldType)) {
            return getBooleanFilter(originalKey, value);
        }
        if (isStringFilter(fieldType)) {
            return getStringFilter(originalKey, value, hasCaseInsensitive);
        }
        if (isNestedStringFilter(fieldType)) {
            return getStringFilter(originalKey, value, false);
        }
        if (isDateTimeFilter(fieldType)) {
            return getDateTimeFilter(originalKey, value);
        }
        if (isIntFilter(fieldType)) {
            return getIntFilter(originalKey, value);
        }
        if (isFloatFilter(fieldType)) {
            return getFloatFilter(originalKey, value);
        }
        if (isEnumFilter(fieldType)) {
            return getEnumFilter(originalKey, value);
        }
    }
    if (isArray(value)) {
        if (isStringFilter(fieldType)) {
            return {
                OR: value.map(function (v) {
                    return getStringFilter(originalKey, v, hasCaseInsensitive);
                }),
            };
        }
        if (isNestedStringFilter(fieldType)) {
            return {
                OR: value.map(function (v) { return getStringFilter(originalKey, v, false); }),
            };
        }
        if (isIntFilter(fieldType)) {
            return { OR: value.map(function (v) { return getIntFilter(originalKey, v); }) };
        }
        if (isDateTimeFilter(fieldType)) {
            return { OR: value.map(function (v) { return getDateTimeFilter(originalKey, v); }) };
        }
        if (isFloatFilter(fieldType)) {
            return { OR: value.map(function (v) { return getFloatFilter(originalKey, v); }) };
        }
        if (isEnumFilter(fieldType)) {
            return { OR: value.map(function (v) { return getEnumFilter(originalKey, v); }) };
        }
    }
    if (fieldType.kind === "INPUT_OBJECT") {
        // we asume for the moment that this is always a relation
        var inputObjectType = context.introspectionResults.types.find(function (t) { return t.name === fieldType.name; });
        if (
        // aorFetchType === GET_MANY_REFERENCE &&
        context.options.queryDialect === "typegraphql" &&
            fieldType.name.includes("RelationFilter")) {
            return _b = {},
                _b[originalKey] = {
                    is: {
                        id: {
                            equals: value,
                        },
                    },
                },
                _b;
        }
        if (!isObject(value)) {
            var hasSomeFilter = inputObjectType.inputFields.some(function (s) { return s.name === "some"; });
            if (hasSomeFilter) {
                return _c = {},
                    _c[originalKey] = {
                        some: {
                            id: {
                                equals: value,
                            },
                        },
                    },
                    _c;
            }
            return _d = {},
                _d[originalKey] = {
                    id: {
                        equals: value,
                    },
                },
                _d;
        }
        if (isArray(value)) {
            var hasSomeFilter = inputObjectType.inputFields.some(function (s) { return s.name === "some"; });
            if (hasSomeFilter) {
                return _e = {},
                    _e[originalKey] = {
                        some: {
                            id: {
                                in: value,
                            },
                        },
                    },
                    _e;
            }
            return _f = {},
                _f[originalKey] = {
                    id: {
                        in: value,
                    },
                },
                _f;
        }
        else {
            // its something nested
            var where = buildWhereWithType(value, inputObjectType, context, aorFetchType);
            return _g = {}, _g[originalKey] = where, _g;
        }
    }
    return _h = {}, _h[originalKey] = value, _h;
};
var buildWhereWithType = function (filter, whereType, context, aorFetchType) {
    var _a, _b;
    var hasAnd = whereType.inputFields.some(function (i) { return i.name === "AND"; });
    var where = hasAnd
        ? Object.keys(filter !== null && filter !== void 0 ? filter : {}).reduce(function (acc, keyRaw) {
            var value = filter[keyRaw];
            var key = sanitizeKey(keyRaw);
            // defaults to AND
            var filters = getFilters(key, value, whereType, context, aorFetchType);
            return __assign(__assign({}, acc), { AND: __spreadArray(__spreadArray([], acc.AND, true), [filters], false) });
        }, { AND: [] })
        : Object.keys(filter !== null && filter !== void 0 ? filter : {}).reduce(function (acc, keyRaw) {
            var value = filter[keyRaw];
            var key = sanitizeKey(keyRaw);
            var filters = getFilters(key, value, whereType, context, aorFetchType);
            return __assign(__assign({}, acc), filters);
        }, {});
    // simplify AND if there is only one
    if (((_a = where.AND) === null || _a === void 0 ? void 0 : _a.length) === 0) {
        delete where.AND;
    }
    if (((_b = where.AND) === null || _b === void 0 ? void 0 : _b.length) === 1) {
        var singleAnd = where.AND[0];
        delete where.AND;
        return __assign(__assign({}, where), singleAnd);
    }
    return where;
};
export var buildWhere = function (filter, context, aorFetchType) {
    var whereType = context.introspectionResults.types.find(function (t) { return t.name === context.resource.type.name + "WhereInput"; });
    return buildWhereWithType(filter, whereType, context, aorFetchType);
};
//# sourceMappingURL=buildWhere.js.map