import set from "lodash/set";
function getOrderType(_a) {
    var resource = _a.resource, introspectionResults = _a.introspectionResults;
    var withoutRelation = resource.type.name + "OrderByInput";
    var withRelation = resource.type.name + "OrderByWithRelationInput";
    return introspectionResults.types.find(function (t) { return t.name === withRelation || t.name === withoutRelation; });
}
export var buildOrderBy = function (sort, context) {
    if (!sort)
        return null;
    var field = sort.field;
    var orderType = getOrderType(context);
    var fieldParts = field === null || field === void 0 ? void 0 : field.split(".");
    if (!orderType || !fieldParts) {
        return null;
    }
    if (!orderType.inputFields.some(function (f) { return f.name === fieldParts[0]; })) {
        return null;
    }
    var selector = {};
    set(selector, field, sort.order === "ASC" ? "asc" : "desc");
    return [selector];
};
//# sourceMappingURL=buildOrderBy.js.map