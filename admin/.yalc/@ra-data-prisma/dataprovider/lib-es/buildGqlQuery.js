var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
import { parse, TypeKind, } from "graphql";
import { QUERY_TYPES } from "ra-data-graphql";
import { DELETE, GET_LIST, GET_MANY, GET_MANY_REFERENCE } from "react-admin";
import { defaultOurOptions } from "./buildDataProvider";
import { isDeprecatedDocumentNodeFragment, } from "./types";
import upperFirst from "lodash/upperFirst";
import getFinalType from "./utils/getFinalType";
import * as gqlTypes from "./utils/gqlTypes";
import isList from "./utils/isList";
import isRequired from "./utils/isRequired";
export var buildFields = function (introspectionResults) {
    return function (fields, maxNestingDepth, depth) {
        if (maxNestingDepth === void 0) { maxNestingDepth = 1; }
        if (depth === void 0) { depth = 0; }
        return fields.reduce(function (acc, field) {
            var type = getFinalType(field.type);
            if (type.name.startsWith("_")) {
                return acc;
            }
            // skip if the field has mandatory args
            if (field.args.some(function (arg) { return arg.type.kind === "NON_NULL"; })) {
                return acc;
            }
            if (type.kind !== TypeKind.OBJECT) {
                return __spreadArray(__spreadArray([], acc, true), [gqlTypes.field(gqlTypes.name(field.name))], false);
            }
            var linkedResource = introspectionResults.resources.find(function (r) { return r.type.name === type.name; });
            if (linkedResource) {
                return __spreadArray(__spreadArray([], acc, true), [
                    gqlTypes.field(gqlTypes.name(field.name), {
                        selectionSet: gqlTypes.selectionSet([
                            gqlTypes.field(gqlTypes.name("id")),
                        ]),
                    }),
                ], false);
            }
            var linkedType = introspectionResults.types.find(function (t) { return t.name === type.name; });
            // linkedtypes  (= nested objects) are currently a bit problematic
            // it is handy to have them, but they be slow to fetch
            // we therefore limit the depth
            if (linkedType && depth < maxNestingDepth) {
                return __spreadArray(__spreadArray([], acc, true), [
                    gqlTypes.field(gqlTypes.name(field.name), {
                        selectionSet: gqlTypes.selectionSet(buildFields(introspectionResults)(linkedType.fields, maxNestingDepth, depth + 1)),
                    }),
                ], false);
            }
            return acc;
        }, []);
    };
};
export var getArgType = function (arg) {
    var type = getFinalType(arg.type);
    var required = isRequired(arg.type);
    var list = isList(arg.type);
    if (list) {
        if (required) {
            return gqlTypes.listType(gqlTypes.nonNullType(gqlTypes.namedType(gqlTypes.name(type.name))));
        }
        return gqlTypes.listType(gqlTypes.namedType(gqlTypes.name(type.name)));
    }
    if (required) {
        return gqlTypes.nonNullType(gqlTypes.namedType(gqlTypes.name(type.name)));
    }
    return gqlTypes.namedType(gqlTypes.name(type.name));
};
export var buildArgs = function (query, variables) {
    if (variables === void 0) { variables = {}; }
    if (query.args.length === 0) {
        return [];
    }
    var validVariables = Object.keys(variables).filter(function (k) { return typeof variables[k] !== "undefined"; });
    return query.args
        .filter(function (arg) { return validVariables.includes(arg.name); })
        .reduce(function (acc, arg) { return __spreadArray(__spreadArray([], acc, true), [
        gqlTypes.argument(gqlTypes.name(arg.name), gqlTypes.variable(gqlTypes.name(arg.name))),
    ], false); }, []);
};
export var buildApolloArgs = function (query, variables) {
    if (variables === void 0) { variables = {}; }
    if (query.args.length === 0) {
        return [];
    }
    var validVariables = Object.keys(variables).filter(function (k) { return typeof variables[k] !== "undefined"; });
    return query.args
        .filter(function (arg) { return validVariables.includes(arg.name); })
        .reduce(function (acc, arg) { return __spreadArray(__spreadArray([], acc, true), [
        gqlTypes.variableDefinition(gqlTypes.variable(gqlTypes.name(arg.name)), getArgType(arg)),
    ], false); }, []);
};
//TODO: validate fragment against the schema
var buildFieldsFromFragment = function (fragment, resourceName, fetchType) {
    var _a;
    var parsedFragment = {};
    if (typeof fragment === "object" &&
        fragment.kind &&
        fragment.kind === "Document") {
        parsedFragment = fragment;
    }
    if (typeof fragment === "string") {
        if (!fragment.startsWith("fragment")) {
            fragment = "fragment tmp on " + resourceName + " " + fragment;
        }
        try {
            parsedFragment = parse(fragment);
        }
        catch (e) {
            throw new Error("Invalid fragment given for resource '" + resourceName + "' and fetchType '" + fetchType + "' (" + e.message + ").");
        }
    }
    return (_a = parsedFragment.definitions) === null || _a === void 0 ? void 0 : _a[0].selectionSet.selections;
};
var filterFields = function (fields, fragment) {
    return fields.filter(function (f) {
        var match = fragment.fields.includes(f.name.value);
        if (fragment.type === "whitelist") {
            // only include these
            return match;
        }
        else {
            // blacklist, exclude these
            return !match;
        }
    });
};
var buildFieldsForFragment = function (resource, aorFetchType, fragment, buildDefaultFields) {
    if (isDeprecatedDocumentNodeFragment(fragment)) {
        console.warn("defining document fragment directly is deprecated, use `type: 'document', doc: gql`...`` instead");
        return buildFieldsFromFragment(fragment, resource.type.name, aorFetchType);
    }
    if (fragment.type === "document") {
        var fragmentFields_1 = buildFieldsFromFragment(fragment.doc, resource.type.name, aorFetchType);
        if (fragment.mode === "extend") {
            var defaultFields = buildDefaultFields();
            var filteredDefaultFields = defaultFields.filter(function (field) {
                return !fragmentFields_1.some(function (ff) { return ff.kind === "Field" && ff.name.value === field.name.value; });
            });
            return __spreadArray(__spreadArray([], filteredDefaultFields, true), fragmentFields_1, true);
        }
        return fragmentFields_1; // replace
    }
    /** blaclist or whitelist**/
    return filterFields(buildDefaultFields(), fragment);
};
export default (function (introspectionResults, options) {
    if (options === void 0) { options = defaultOurOptions; }
    return function (resource, aorFetchType, variables, fragment) {
        var queryType = resource[aorFetchType];
        var _a = options.queryDialect, queryDialect = _a === void 0 ? "nexus-prisma" : _a;
        if (!queryType) {
            throw new Error("No query or mutation matching aor fetch type " + aorFetchType + " could be found for resource " + resource.type.name);
        }
        var orderBy = variables.orderBy, skip = variables.skip, take = variables.take, countVariables = __rest(variables, ["orderBy", "skip", "take"]);
        var apolloArgs = buildApolloArgs(queryType, variables);
        var args = buildArgs(queryType, variables);
        var countArgs = buildArgs(queryType, countVariables);
        var buildDefaultFields = function () {
            return buildFields(introspectionResults)(resource.type.fields);
        };
        var fields = fragment
            ? buildFieldsForFragment(resource, aorFetchType, fragment, buildDefaultFields)
            : buildDefaultFields();
        var totals = {
            "nexus-prisma": function () {
                var _a;
                var countName = queryType.name + "Count";
                var supportsCountArgs = ((_a = introspectionResults.queries.find(function (query) { return query.name === countName; })) === null || _a === void 0 ? void 0 : _a.args.length) > 0;
                return gqlTypes.field(gqlTypes.name(countName), {
                    alias: gqlTypes.name("total"),
                    arguments: supportsCountArgs ? countArgs : undefined,
                });
            },
            typegraphql: function () {
                var resourceName = upperFirst(resource.type.name);
                var aggregateType = introspectionResults.types.find(function (n) { return n.name === "Aggregate" + resourceName; });
                // older versions use "count", newer "_count"
                var countName = aggregateType.kind === "OBJECT" &&
                    aggregateType.fields.find(function (f) { return f.name === "_count"; })
                    ? "_count"
                    : "count";
                return gqlTypes.field(gqlTypes.name("aggregate" + resourceName), {
                    alias: gqlTypes.name("total"),
                    arguments: countArgs,
                    selectionSet: gqlTypes.selectionSet([
                        gqlTypes.field(gqlTypes.name(countName), {
                            selectionSet: gqlTypes.selectionSet([
                                gqlTypes.field(gqlTypes.name("_all")),
                            ]),
                        }),
                    ]),
                });
            },
        };
        if (aorFetchType === GET_LIST ||
            aorFetchType === GET_MANY ||
            aorFetchType === GET_MANY_REFERENCE) {
            return gqlTypes.document([
                gqlTypes.operationDefinition("query", gqlTypes.selectionSet([
                    gqlTypes.field(gqlTypes.name(queryType.name), {
                        alias: gqlTypes.name("items"),
                        arguments: args,
                        selectionSet: gqlTypes.selectionSet(fields),
                    }),
                    totals[queryDialect](),
                ]), gqlTypes.name(queryType.name), apolloArgs),
            ]);
        }
        if (aorFetchType === DELETE) {
            return gqlTypes.document([
                gqlTypes.operationDefinition("mutation", gqlTypes.selectionSet([
                    gqlTypes.field(gqlTypes.name(queryType.name), {
                        alias: gqlTypes.name("data"),
                        arguments: args,
                        selectionSet: gqlTypes.selectionSet([
                            gqlTypes.field(gqlTypes.name("id")),
                        ]),
                    }),
                ]), gqlTypes.name(queryType.name), apolloArgs),
            ]);
        }
        return gqlTypes.document([
            gqlTypes.operationDefinition(QUERY_TYPES.includes(aorFetchType) ? "query" : "mutation", gqlTypes.selectionSet([
                gqlTypes.field(gqlTypes.name(queryType.name), {
                    alias: gqlTypes.name("data"),
                    arguments: args,
                    selectionSet: gqlTypes.selectionSet(fields),
                }),
            ]), gqlTypes.name(queryType.name), apolloArgs),
        ]);
    };
});
//# sourceMappingURL=buildGqlQuery.js.map