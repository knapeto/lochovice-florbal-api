import { IntrospectionResult, Resource } from "./constants/interfaces";
import { FetchType, QueryDialect } from "./types";
declare const _default: (introspectionResults: IntrospectionResult, { queryDialect }: {
    queryDialect: QueryDialect;
}) => (aorFetchType: FetchType, resource: Resource) => (response: {
    [key: string]: any;
}) => {
    data: any;
    total: any;
} | {
    data: any;
    total?: undefined;
};
export default _default;
//# sourceMappingURL=getResponseParser.d.ts.map