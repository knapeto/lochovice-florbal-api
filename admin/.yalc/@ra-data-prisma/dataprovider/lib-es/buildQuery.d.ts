import { IntrospectionResult } from "./constants/interfaces";
import { FetchType, OurOptions } from "./types";
export declare const buildQueryFactory: (introspectionResults: IntrospectionResult, options: OurOptions) => (aorFetchType: FetchType, resourceName: string, params: any) => {
    query: import("graphql").DocumentNode;
    variables: {
        skip: number;
        take: number;
        orderBy: {}[];
        where: any;
        aorFetchType: FetchType;
    } | {
        data: any;
    } | {
        where: {
            id: any;
        };
    };
    parseResponse: (response: {
        [key: string]: any;
    }) => {
        data: any;
        total: any;
    } | {
        data: any;
        total?: undefined;
    };
};
//# sourceMappingURL=buildQuery.d.ts.map