import { LegacyDataProvider } from "react-admin";
import { Options, OurOptions } from "./types";
export declare const defaultOurOptions: OurOptions;
export declare const defaultOptions: Options;
declare const buildDataProvider: (options: Options) => Promise<LegacyDataProvider>;
export default buildDataProvider;
//# sourceMappingURL=buildDataProvider.d.ts.map