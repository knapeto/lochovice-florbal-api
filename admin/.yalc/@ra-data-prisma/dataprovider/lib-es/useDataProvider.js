var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import buildDataProvider, { defaultOurOptions } from "./buildDataProvider";
import { useEffect, useState } from "react";
var useDataProvider = function (options) {
    var _a = useState(), dataProvider = _a[0], setDataProvider = _a[1];
    useEffect(function () {
        buildDataProvider(__assign(__assign({}, defaultOurOptions), options)).then(function (p) {
            setDataProvider(function () { return p; });
        });
    }, []);
    return dataProvider;
};
export default useDataProvider;
//# sourceMappingURL=useDataProvider.js.map