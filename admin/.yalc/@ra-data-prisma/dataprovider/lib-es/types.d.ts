import { CREATE, DELETE, GET_LIST, GET_MANY, GET_MANY_REFERENCE, GET_ONE, UPDATE } from "react-admin";
import { DocumentNode } from "graphql";
import { Resource } from "./constants/interfaces";
export declare type WhiteListFragment = {
    type: "whitelist";
    fields: string[];
};
export declare type BlackListFragment = {
    type: "blacklist";
    fields: string[];
};
export declare const isDeprecatedDocumentNodeFragment: (fragment: ResourceFragment) => fragment is DocumentNode;
export declare const isOneAndManyFragment: (fragment: ResourceViewFragment) => fragment is DoubleFragment;
export declare type DocumentFragment = {
    type: "document";
    doc: DocumentNode;
    mode?: "replace" | "extend";
};
export declare type ResourceFragment = DocumentNode | DocumentFragment | WhiteListFragment | BlackListFragment;
export declare type DoubleFragment = {
    one?: ResourceFragment;
    many?: ResourceFragment;
};
export declare type ResourceViewFragment = ResourceFragment | DoubleFragment;
export declare type ResourceView = {
    resource: string;
    fragment: ResourceViewFragment;
};
export declare type MutationFetchType = typeof CREATE | typeof DELETE | typeof UPDATE;
export declare type MutationOperationNameMap = {
    [K in MutationFetchType]: (resource: Resource) => string;
};
export declare type MutationOperationNames = Partial<Record<QueryDialect, MutationOperationNameMap>>;
export declare type QueryDialect = "nexus-prisma" | "typegraphql";
declare type Filter = Record<string, unknown>;
export declare type CustomizeInputDataFunction = (data: Record<string, any>, rawParams: Record<string, any>) => Record<string, unknown>;
export declare type CustomizeInputData = {
    [name: string]: {
        create?: CustomizeInputDataFunction;
        update?: CustomizeInputDataFunction;
    };
};
export declare type IntrospectionOptions = {
    schema?: any;
};
export declare type ConfigOptions = {
    resourceViews?: {
        [name: string]: ResourceView;
    };
    aliasPrefix?: string;
    filters?: {
        [filterName: string]: (value: any) => Filter | void;
    };
    customizeInputData?: CustomizeInputData;
    introspection?: IntrospectionOptions;
    mutationOperationNames?: MutationOperationNames;
};
export declare type QueryFetchType = typeof GET_LIST | typeof GET_MANY | typeof GET_MANY_REFERENCE | typeof GET_ONE;
export declare type FetchType = QueryFetchType | MutationFetchType;
export declare type VariantOptions = {
    queryDialect?: QueryDialect;
};
export declare type OurOptions = ConfigOptions & VariantOptions;
declare type RaDataGraphqlOptions = {
    clientOptions?: {
        uri: string;
    };
    client?: any;
};
export declare type Options = RaDataGraphqlOptions & ConfigOptions & VariantOptions;
export declare type DataProviderOptions = RaDataGraphqlOptions & ConfigOptions & Partial<VariantOptions>;
export {};
//# sourceMappingURL=types.d.ts.map