var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
import merge from "lodash/merge";
import buildRaGraphqlDataProvider from "ra-data-graphql";
import { DELETE, DELETE_MANY, UPDATE, UPDATE_MANY, } from "react-admin";
import { buildQueryFactory } from "./buildQuery";
import { makeIntrospectionOptions } from "./utils/makeIntrospectionOptions";
export var defaultOurOptions = {
    queryDialect: "nexus-prisma",
};
export var defaultOptions = __assign({ clientOptions: { uri: "/graphql" } }, defaultOurOptions);
var buildDataProvider = function (options) {
    var fullOptions = merge({}, defaultOptions, options);
    return buildRaGraphqlDataProvider(merge({}, {
        buildQuery: buildQueryFactory,
        introspection: makeIntrospectionOptions(fullOptions),
    }, fullOptions)).then(function (graphQLDataProvider) {
        return function (fetchType, resource, params) {
            // Temporary work-around until we make use of updateMany and deleteMany mutations
            if (fetchType === DELETE_MANY) {
                var ids = params.ids, otherParams_1 = __rest(params, ["ids"]);
                return Promise.all(params.ids.map(function (id) {
                    return graphQLDataProvider(DELETE, resource, __assign({ id: id }, otherParams_1));
                })).then(function (results) {
                    return { data: results.map(function (_a) {
                            var data = _a.data;
                            return data.id;
                        }) };
                });
            }
            if (fetchType === UPDATE_MANY) {
                var ids = params.ids, otherParams_2 = __rest(params, ["ids"]);
                return Promise.all(params.ids.map(function (id) {
                    return graphQLDataProvider(UPDATE, resource, __assign({ id: id }, otherParams_2));
                })).then(function (results) {
                    return { data: results.map(function (_a) {
                            var data = _a.data;
                            return data.id;
                        }) };
                });
            }
            return graphQLDataProvider(fetchType, resource, params);
        };
    });
};
export default buildDataProvider;
//# sourceMappingURL=buildDataProvider.js.map