var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { TypeKind } from "graphql";
import { GET_LIST, GET_MANY, GET_MANY_REFERENCE } from "react-admin";
import getFinalType from "./utils/getFinalType";
var sanitizeResource = function (introspectionResults, resource) {
    return function (data) {
        if (data === void 0) { data = {}; }
        return Object.keys(data).reduce(function (acc, key) {
            var _a, _b, _c, _d;
            var _e, _f;
            if (key.startsWith("_")) {
                return acc;
            }
            var field = resource.type.fields.find(function (f) { return f.name === key; });
            var type = getFinalType(field.type);
            if (type.kind !== TypeKind.OBJECT) {
                return __assign(__assign({}, acc), (_a = {}, _a[field.name] = data[field.name], _a));
            }
            // if the field contains an array of object with ids, we add a field field_ids to the data
            if (Array.isArray(data[field.name]) &&
                ((_e = data[field.name]) === null || _e === void 0 ? void 0 : _e.every(function (c) { return c.id; }))) {
                return __assign(__assign({}, acc), (_b = {}, _b[field.name] = data[field.name], _b[field.name + "_ids"] = data[field.name].map(function (c) { return c.id; }), _b));
            }
            // similarly if its an object with id
            if ((_f = data[field.name]) === null || _f === void 0 ? void 0 : _f.id) {
                return __assign(__assign({}, acc), (_c = {}, _c[field.name] = data[field.name], _c[field.name + "_id"] = data[field.name].id, _c));
            }
            return __assign(__assign({}, acc), (_d = {}, _d[field.name] = data[field.name], _d));
        }, {});
    };
};
export default (function (introspectionResults, _a) {
    var queryDialect = _a.queryDialect;
    return function (aorFetchType, resource) {
        return function (response) {
            var sanitize = sanitizeResource(introspectionResults, resource);
            var data = response.data;
            var getTotal = function () {
                var _a;
                switch (queryDialect) {
                    case "nexus-prisma":
                        return response.data.total;
                    case "typegraphql":
                        return ((_a = response.data.total._count) !== null && _a !== void 0 ? _a : response.data.total.count)._all;
                }
            };
            if (aorFetchType === GET_LIST ||
                aorFetchType === GET_MANY ||
                aorFetchType === GET_MANY_REFERENCE) {
                return {
                    data: response.data.items.map(sanitize),
                    total: getTotal(),
                };
            }
            return { data: sanitize(data.data) };
        };
    };
});
//# sourceMappingURL=getResponseParser.js.map