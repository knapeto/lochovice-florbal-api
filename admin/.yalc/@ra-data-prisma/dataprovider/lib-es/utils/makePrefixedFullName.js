import upperFirst from "lodash/upperFirst";
export var makePrefixedFullName = function (name, prefix) {
    return !prefix ? name : prefix + upperFirst(name);
};
//# sourceMappingURL=makePrefixedFullName.js.map