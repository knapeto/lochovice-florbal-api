var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { Kind, } from "graphql";
// Functional utils to easily build GraphQL ASTs
// Inspired by https://github.com/imranolas/graphql-ast-types
export var document = function (definitions) { return ({
    kind: Kind.DOCUMENT,
    definitions: definitions,
}); };
export var operationDefinition = function (operation, selectionSet, name, variableDefinitions) { return ({
    kind: Kind.OPERATION_DEFINITION,
    operation: operation,
    selectionSet: selectionSet,
    name: name,
    variableDefinitions: variableDefinitions,
}); };
export var selectionSet = function (selections) { return ({
    kind: Kind.SELECTION_SET,
    selections: selections,
}); };
export var field = function (name, optionalValues) {
    if (optionalValues === void 0) { optionalValues = {}; }
    return (__assign({ kind: Kind.FIELD, name: name }, optionalValues));
};
export var listType = function (type) { return ({
    kind: Kind.LIST_TYPE,
    type: type,
}); };
export var nonNullType = function (type) { return ({
    kind: Kind.NON_NULL_TYPE,
    type: type,
}); };
export var variableDefinition = function (variable, type) { return ({
    kind: Kind.VARIABLE_DEFINITION,
    variable: variable,
    type: type,
}); };
export var variable = function (name) { return ({
    kind: Kind.VARIABLE,
    name: name,
}); };
export var name = function (value) { return ({
    kind: Kind.NAME,
    value: value,
}); };
export var namedType = function (name) { return ({
    kind: Kind.NAMED_TYPE,
    name: name,
}); };
export var argument = function (name, value) { return ({
    kind: Kind.ARGUMENT,
    name: name,
    value: value,
}); };
//# sourceMappingURL=gqlTypes.js.map