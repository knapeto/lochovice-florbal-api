var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import camelCase from "lodash/camelCase";
import pluralize from "pluralize";
import { CREATE, DELETE, GET_LIST, GET_MANY, GET_MANY_REFERENCE, GET_ONE, UPDATE, } from "react-admin";
import { makePrefixedFullName } from "./makePrefixedFullName";
export var makeIntrospectionOptions = function (options) {
    var _a, _b, _c;
    var prefix = function (s) { return makePrefixedFullName(s, options === null || options === void 0 ? void 0 : options.aliasPrefix); };
    var mutationOperationNames = __assign({ "nexus-prisma": (_a = {},
            _a[CREATE] = function (resource) { return prefix("createOne" + resource.name); },
            _a[UPDATE] = function (resource) { return prefix("updateOne" + resource.name); },
            _a[DELETE] = function (resource) { return prefix("deleteOne" + resource.name); },
            _a), typegraphql: (_b = {},
            _b[CREATE] = function (resource) { return prefix("create" + resource.name); },
            _b[UPDATE] = function (resource) { return prefix("update" + resource.name); },
            _b[DELETE] = function (resource) { return prefix("delete" + resource.name); },
            _b) }, options.mutationOperationNames);
    return __assign({ operationNames: __assign((_c = {}, _c[GET_LIST] = function (resource) {
            return prefix("" + pluralize(camelCase(resource.name)));
        }, _c[GET_ONE] = function (resource) { return prefix("" + camelCase(resource.name)); }, _c[GET_MANY] = function (resource) {
            return prefix("" + pluralize(camelCase(resource.name)));
        }, _c[GET_MANY_REFERENCE] = function (resource) {
            return prefix("" + pluralize(camelCase(resource.name)));
        }, _c), mutationOperationNames[options.queryDialect]), exclude: undefined, include: undefined }, options.introspection);
};
//# sourceMappingURL=makeIntrospectionOptions.js.map