export default function exhaust(value, message) {
    if (message === void 0) { message = "No such case in exhaustive switch: " + value; }
    throw new Error(message);
}
//# sourceMappingURL=exhaust.js.map