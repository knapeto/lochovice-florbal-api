import { Resource } from "../constants/interfaces";
import { OurOptions } from "../types";
export declare const makeIntrospectionOptions: (options: OurOptions) => {
    schema?: any;
    operationNames: {
        GET_LIST: (resource: Resource) => string;
        GET_ONE: (resource: Resource) => string;
        GET_MANY: (resource: Resource) => string;
        GET_MANY_REFERENCE: (resource: Resource) => string;
    };
    exclude: any;
    include: any;
};
//# sourceMappingURL=makeIntrospectionOptions.d.ts.map