export var PRISMA_CONNECT = "connect";
export var PRISMA_DISCONNECT = "disconnect";
export var PRISMA_CREATE = "create";
export var PRISMA_DELETE = "delete";
export var PRISMA_UPDATE = "update";
//# sourceMappingURL=mutations.js.map