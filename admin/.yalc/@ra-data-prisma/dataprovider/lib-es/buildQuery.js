import { GET_LIST, GET_MANY, GET_MANY_REFERENCE } from "react-admin";
import buildGqlQuery from "./buildGqlQuery";
import { buildVariables } from "./buildVariables";
import getResponseParser from "./getResponseParser";
import { isOneAndManyFragment, } from "./types";
var MANY_FETCH_TYPES = [GET_LIST, GET_MANY, GET_MANY_REFERENCE];
export var buildQueryFactory = function (introspectionResults, options) {
    var resourceViews = options.resourceViews;
    var knownResources = introspectionResults.resources.map(function (r) { return r.type.name; });
    return function (aorFetchType, resourceName, params) {
        var resourceView = resourceViews === null || resourceViews === void 0 ? void 0 : resourceViews[resourceName];
        var resourceNameToUse = resourceView
            ? resourceView.resource
            : resourceName;
        var resource = introspectionResults.resources.find(function (r) { return r.type.name === resourceNameToUse; });
        if (!resource) {
            throw new Error("Unknown resource " + resourceNameToUse + ". Make sure it has been declared on your server side schema. Known resources are " + knownResources.join(", ") + ". " + (resourceViews
                ? "Known view resources are " + Object.keys(resourceViews).join(", ")
                : ""));
        }
        var resourceViewFragment = undefined;
        if (resourceView) {
            if (isOneAndManyFragment(resourceView.fragment)) {
                // has one or many or both
                // if only one is defined, fall back to no fragment for the other
                if (MANY_FETCH_TYPES.includes(aorFetchType)) {
                    if (resourceView.fragment.many) {
                        resourceViewFragment = resourceView.fragment.many;
                    }
                }
                else {
                    if (resourceView.fragment.one) {
                        resourceViewFragment = resourceView.fragment.one;
                    }
                }
            }
            else {
                resourceViewFragment = resourceView.fragment;
            }
        }
        var variables = buildVariables({
            introspectionResults: introspectionResults,
            options: options,
            resource: resource,
        })(aorFetchType, params);
        if (options.queryDialect === "typegraphql") {
            Object.keys(variables).forEach(function (key) {
                var _a;
                variables[key] = (_a = variables[key]) !== null && _a !== void 0 ? _a : undefined;
            });
        }
        var query = buildGqlQuery(introspectionResults, options)(resource, aorFetchType, variables, resourceViewFragment);
        var parseResponse = getResponseParser(introspectionResults, {
            queryDialect: options.queryDialect,
        })(aorFetchType, resource);
        return {
            query: query,
            variables: variables,
            parseResponse: parseResponse,
        };
    };
};
//# sourceMappingURL=buildQuery.js.map