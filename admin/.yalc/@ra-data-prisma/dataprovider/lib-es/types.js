export var isDeprecatedDocumentNodeFragment = function (fragment) {
    return "kind" in fragment && fragment.kind === "Document";
};
export var isOneAndManyFragment = function (fragment) {
    return "one" in fragment || "many" in fragment;
};
//# sourceMappingURL=types.js.map