"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildApolloArgs = exports.buildArgs = exports.getArgType = exports.buildFields = void 0;
var graphql_1 = require("graphql");
var ra_data_graphql_1 = require("ra-data-graphql");
var react_admin_1 = require("react-admin");
var buildDataProvider_1 = require("./buildDataProvider");
var types_1 = require("./types");
var upperFirst_1 = __importDefault(require("lodash/upperFirst"));
var getFinalType_1 = __importDefault(require("./utils/getFinalType"));
var gqlTypes = __importStar(require("./utils/gqlTypes"));
var isList_1 = __importDefault(require("./utils/isList"));
var isRequired_1 = __importDefault(require("./utils/isRequired"));
var buildFields = function (introspectionResults) {
    return function (fields, maxNestingDepth, depth) {
        if (maxNestingDepth === void 0) { maxNestingDepth = 1; }
        if (depth === void 0) { depth = 0; }
        return fields.reduce(function (acc, field) {
            var type = (0, getFinalType_1.default)(field.type);
            if (type.name.startsWith("_")) {
                return acc;
            }
            // skip if the field has mandatory args
            if (field.args.some(function (arg) { return arg.type.kind === "NON_NULL"; })) {
                return acc;
            }
            if (type.kind !== graphql_1.TypeKind.OBJECT) {
                return __spreadArray(__spreadArray([], acc, true), [gqlTypes.field(gqlTypes.name(field.name))], false);
            }
            var linkedResource = introspectionResults.resources.find(function (r) { return r.type.name === type.name; });
            if (linkedResource) {
                return __spreadArray(__spreadArray([], acc, true), [
                    gqlTypes.field(gqlTypes.name(field.name), {
                        selectionSet: gqlTypes.selectionSet([
                            gqlTypes.field(gqlTypes.name("id")),
                        ]),
                    }),
                ], false);
            }
            var linkedType = introspectionResults.types.find(function (t) { return t.name === type.name; });
            // linkedtypes  (= nested objects) are currently a bit problematic
            // it is handy to have them, but they be slow to fetch
            // we therefore limit the depth
            if (linkedType && depth < maxNestingDepth) {
                return __spreadArray(__spreadArray([], acc, true), [
                    gqlTypes.field(gqlTypes.name(field.name), {
                        selectionSet: gqlTypes.selectionSet((0, exports.buildFields)(introspectionResults)(linkedType.fields, maxNestingDepth, depth + 1)),
                    }),
                ], false);
            }
            return acc;
        }, []);
    };
};
exports.buildFields = buildFields;
var getArgType = function (arg) {
    var type = (0, getFinalType_1.default)(arg.type);
    var required = (0, isRequired_1.default)(arg.type);
    var list = (0, isList_1.default)(arg.type);
    if (list) {
        if (required) {
            return gqlTypes.listType(gqlTypes.nonNullType(gqlTypes.namedType(gqlTypes.name(type.name))));
        }
        return gqlTypes.listType(gqlTypes.namedType(gqlTypes.name(type.name)));
    }
    if (required) {
        return gqlTypes.nonNullType(gqlTypes.namedType(gqlTypes.name(type.name)));
    }
    return gqlTypes.namedType(gqlTypes.name(type.name));
};
exports.getArgType = getArgType;
var buildArgs = function (query, variables) {
    if (variables === void 0) { variables = {}; }
    if (query.args.length === 0) {
        return [];
    }
    var validVariables = Object.keys(variables).filter(function (k) { return typeof variables[k] !== "undefined"; });
    return query.args
        .filter(function (arg) { return validVariables.includes(arg.name); })
        .reduce(function (acc, arg) { return __spreadArray(__spreadArray([], acc, true), [
        gqlTypes.argument(gqlTypes.name(arg.name), gqlTypes.variable(gqlTypes.name(arg.name))),
    ], false); }, []);
};
exports.buildArgs = buildArgs;
var buildApolloArgs = function (query, variables) {
    if (variables === void 0) { variables = {}; }
    if (query.args.length === 0) {
        return [];
    }
    var validVariables = Object.keys(variables).filter(function (k) { return typeof variables[k] !== "undefined"; });
    return query.args
        .filter(function (arg) { return validVariables.includes(arg.name); })
        .reduce(function (acc, arg) { return __spreadArray(__spreadArray([], acc, true), [
        gqlTypes.variableDefinition(gqlTypes.variable(gqlTypes.name(arg.name)), (0, exports.getArgType)(arg)),
    ], false); }, []);
};
exports.buildApolloArgs = buildApolloArgs;
//TODO: validate fragment against the schema
var buildFieldsFromFragment = function (fragment, resourceName, fetchType) {
    var _a;
    var parsedFragment = {};
    if (typeof fragment === "object" &&
        fragment.kind &&
        fragment.kind === "Document") {
        parsedFragment = fragment;
    }
    if (typeof fragment === "string") {
        if (!fragment.startsWith("fragment")) {
            fragment = "fragment tmp on " + resourceName + " " + fragment;
        }
        try {
            parsedFragment = (0, graphql_1.parse)(fragment);
        }
        catch (e) {
            throw new Error("Invalid fragment given for resource '" + resourceName + "' and fetchType '" + fetchType + "' (" + e.message + ").");
        }
    }
    return (_a = parsedFragment.definitions) === null || _a === void 0 ? void 0 : _a[0].selectionSet.selections;
};
var filterFields = function (fields, fragment) {
    return fields.filter(function (f) {
        var match = fragment.fields.includes(f.name.value);
        if (fragment.type === "whitelist") {
            // only include these
            return match;
        }
        else {
            // blacklist, exclude these
            return !match;
        }
    });
};
var buildFieldsForFragment = function (resource, aorFetchType, fragment, buildDefaultFields) {
    if ((0, types_1.isDeprecatedDocumentNodeFragment)(fragment)) {
        console.warn("defining document fragment directly is deprecated, use `type: 'document', doc: gql`...`` instead");
        return buildFieldsFromFragment(fragment, resource.type.name, aorFetchType);
    }
    if (fragment.type === "document") {
        var fragmentFields_1 = buildFieldsFromFragment(fragment.doc, resource.type.name, aorFetchType);
        if (fragment.mode === "extend") {
            var defaultFields = buildDefaultFields();
            var filteredDefaultFields = defaultFields.filter(function (field) {
                return !fragmentFields_1.some(function (ff) { return ff.kind === "Field" && ff.name.value === field.name.value; });
            });
            return __spreadArray(__spreadArray([], filteredDefaultFields, true), fragmentFields_1, true);
        }
        return fragmentFields_1; // replace
    }
    /** blaclist or whitelist**/
    return filterFields(buildDefaultFields(), fragment);
};
exports.default = (function (introspectionResults, options) {
    if (options === void 0) { options = buildDataProvider_1.defaultOurOptions; }
    return function (resource, aorFetchType, variables, fragment) {
        var queryType = resource[aorFetchType];
        var _a = options.queryDialect, queryDialect = _a === void 0 ? "nexus-prisma" : _a;
        if (!queryType) {
            throw new Error("No query or mutation matching aor fetch type " + aorFetchType + " could be found for resource " + resource.type.name);
        }
        var orderBy = variables.orderBy, skip = variables.skip, take = variables.take, countVariables = __rest(variables, ["orderBy", "skip", "take"]);
        var apolloArgs = (0, exports.buildApolloArgs)(queryType, variables);
        var args = (0, exports.buildArgs)(queryType, variables);
        var countArgs = (0, exports.buildArgs)(queryType, countVariables);
        var buildDefaultFields = function () {
            return (0, exports.buildFields)(introspectionResults)(resource.type.fields);
        };
        var fields = fragment
            ? buildFieldsForFragment(resource, aorFetchType, fragment, buildDefaultFields)
            : buildDefaultFields();
        var totals = {
            "nexus-prisma": function () {
                var _a;
                var countName = queryType.name + "Count";
                var supportsCountArgs = ((_a = introspectionResults.queries.find(function (query) { return query.name === countName; })) === null || _a === void 0 ? void 0 : _a.args.length) > 0;
                return gqlTypes.field(gqlTypes.name(countName), {
                    alias: gqlTypes.name("total"),
                    arguments: supportsCountArgs ? countArgs : undefined,
                });
            },
            typegraphql: function () {
                var resourceName = (0, upperFirst_1.default)(resource.type.name);
                var aggregateType = introspectionResults.types.find(function (n) { return n.name === "Aggregate" + resourceName; });
                // older versions use "count", newer "_count"
                var countName = aggregateType.kind === "OBJECT" &&
                    aggregateType.fields.find(function (f) { return f.name === "_count"; })
                    ? "_count"
                    : "count";
                return gqlTypes.field(gqlTypes.name("aggregate" + resourceName), {
                    alias: gqlTypes.name("total"),
                    arguments: countArgs,
                    selectionSet: gqlTypes.selectionSet([
                        gqlTypes.field(gqlTypes.name(countName), {
                            selectionSet: gqlTypes.selectionSet([
                                gqlTypes.field(gqlTypes.name("_all")),
                            ]),
                        }),
                    ]),
                });
            },
        };
        if (aorFetchType === react_admin_1.GET_LIST ||
            aorFetchType === react_admin_1.GET_MANY ||
            aorFetchType === react_admin_1.GET_MANY_REFERENCE) {
            return gqlTypes.document([
                gqlTypes.operationDefinition("query", gqlTypes.selectionSet([
                    gqlTypes.field(gqlTypes.name(queryType.name), {
                        alias: gqlTypes.name("items"),
                        arguments: args,
                        selectionSet: gqlTypes.selectionSet(fields),
                    }),
                    totals[queryDialect](),
                ]), gqlTypes.name(queryType.name), apolloArgs),
            ]);
        }
        if (aorFetchType === react_admin_1.DELETE) {
            return gqlTypes.document([
                gqlTypes.operationDefinition("mutation", gqlTypes.selectionSet([
                    gqlTypes.field(gqlTypes.name(queryType.name), {
                        alias: gqlTypes.name("data"),
                        arguments: args,
                        selectionSet: gqlTypes.selectionSet([
                            gqlTypes.field(gqlTypes.name("id")),
                        ]),
                    }),
                ]), gqlTypes.name(queryType.name), apolloArgs),
            ]);
        }
        return gqlTypes.document([
            gqlTypes.operationDefinition(ra_data_graphql_1.QUERY_TYPES.includes(aorFetchType) ? "query" : "mutation", gqlTypes.selectionSet([
                gqlTypes.field(gqlTypes.name(queryType.name), {
                    alias: gqlTypes.name("data"),
                    arguments: args,
                    selectionSet: gqlTypes.selectionSet(fields),
                }),
            ]), gqlTypes.name(queryType.name), apolloArgs),
        ]);
    };
});
//# sourceMappingURL=buildGqlQuery.js.map