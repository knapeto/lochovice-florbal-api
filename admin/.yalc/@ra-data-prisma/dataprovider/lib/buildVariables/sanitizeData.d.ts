import { IntrospectionInputValue } from "graphql";
/**
 * Due to some implementation details in react-admin, we have to add copies with suffixed keys of certain field data.
 *
 * We transform that back here.
 *
 * this function throws if both variants (suffixed and non suffixed is used) because we cannot decide which one takes precedence
 */
export declare const getSanitizedFieldData: (data: Record<string, any>, previousData: Record<string, any>, field: IntrospectionInputValue) => {
    fieldData: any;
    previousFieldData: any;
};
export declare const hasFieldData: (data: Record<string, any>, field: IntrospectionInputValue) => boolean;
//# sourceMappingURL=sanitizeData.d.ts.map