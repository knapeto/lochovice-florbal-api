"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildOrderBy = void 0;
var set_1 = __importDefault(require("lodash/set"));
function getOrderType(_a) {
    var resource = _a.resource, introspectionResults = _a.introspectionResults;
    var withoutRelation = resource.type.name + "OrderByInput";
    var withRelation = resource.type.name + "OrderByWithRelationInput";
    return introspectionResults.types.find(function (t) { return t.name === withRelation || t.name === withoutRelation; });
}
var buildOrderBy = function (sort, context) {
    if (!sort)
        return null;
    var field = sort.field;
    var orderType = getOrderType(context);
    var fieldParts = field === null || field === void 0 ? void 0 : field.split(".");
    if (!orderType || !fieldParts) {
        return null;
    }
    if (!orderType.inputFields.some(function (f) { return f.name === fieldParts[0]; })) {
        return null;
    }
    var selector = {};
    (0, set_1.default)(selector, field, sort.order === "ASC" ? "asc" : "desc");
    return [selector];
};
exports.buildOrderBy = buildOrderBy;
//# sourceMappingURL=buildOrderBy.js.map