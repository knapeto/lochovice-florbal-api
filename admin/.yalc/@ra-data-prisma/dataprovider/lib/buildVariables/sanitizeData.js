"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hasFieldData = exports.getSanitizedFieldData = void 0;
var lodash_1 = require("lodash");
/**
 * Due to some implementation details in react-admin, we have to add copies with suffixed keys of certain field data.
 *
 * We transform that back here.
 *
 * this function throws if both variants (suffixed and non suffixed is used) because we cannot decide which one takes precedence
 */
var getSanitizedFieldData = function (data, previousData, field) {
    var _a;
    var key = field.name;
    var keyWithArraySuffix = key + "_ids";
    console.log(key, data);
    if ((0, lodash_1.has)(data, keyWithArraySuffix)) {
        if ((0, lodash_1.has)(data, key)) {
            // throw new Error(
            //   `cannot update ${key} and ${keyWithArraySuffix} at the same time. Only use one in the form.`,
            // );
        }
        return {
            fieldData: data[keyWithArraySuffix].map(function (id) { return ({ id: id }); }),
            previousFieldData: previousData
                ? (_a = previousData[keyWithArraySuffix]) === null || _a === void 0 ? void 0 : _a.map(function (id) { return ({ id: id }); })
                : null,
        };
    }
    var keyWithIdsSuffix = key + "_id";
    if ((0, lodash_1.has)(data, keyWithIdsSuffix)) {
        if ((0, lodash_1.has)(data, key)) {
            // throw new Error(
            //   `cannot update ${key} and ${keyWithIdsSuffix} at the same time. Only use one in the form.`,
            // );
        }
        return {
            fieldData: {
                id: data[keyWithIdsSuffix],
            },
            previousFieldData: previousData
                ? {
                    id: previousData[keyWithIdsSuffix],
                }
                : null,
        };
    }
    return {
        fieldData: data[key],
        previousFieldData: previousData ? previousData[key] : null,
    };
};
exports.getSanitizedFieldData = getSanitizedFieldData;
var hasFieldData = function (data, field) {
    return ((0, lodash_1.has)(data, field.name) ||
        (0, lodash_1.has)(data, field.name + "_id") ||
        (0, lodash_1.has)(data, field.name + "_ids"));
};
exports.hasFieldData = hasFieldData;
//# sourceMappingURL=sanitizeData.js.map