"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildData = void 0;
var lodash_1 = require("lodash");
var isEqual_1 = __importDefault(require("lodash/isEqual"));
var isNil_1 = __importDefault(require("lodash/isNil"));
var isObject_1 = __importDefault(require("lodash/isObject"));
var exhaust_1 = __importDefault(require("../utils/exhaust"));
var getFinalType_1 = __importDefault(require("../utils/getFinalType"));
var sanitizeData_1 = require("./sanitizeData");
var ModifiersParams;
(function (ModifiersParams) {
    ModifiersParams["connect"] = "connect";
    ModifiersParams["create"] = "create";
    ModifiersParams["createMany"] = "createMany";
    ModifiersParams["delete"] = "delete";
    ModifiersParams["deleteMany"] = "deleteMany";
    ModifiersParams["disconnect"] = "disconnect";
    ModifiersParams["set"] = "set";
    ModifiersParams["update"] = "update";
    ModifiersParams["updateMany"] = "updateMany";
    ModifiersParams["upsert"] = "upsert";
    ModifiersParams["connectOrCreate"] = "connectOrCreate";
})(ModifiersParams || (ModifiersParams = {}));
var getCreateInputDataTypeForList = function (createModifier, context) {
    var createListModifierType = createModifier.type;
    var createInputFieldType = createListModifierType.ofType.kind === "NON_NULL"
        ? createListModifierType.ofType.ofType
        : createListModifierType.ofType;
    return context.introspectionResults.types.find(
    // what about a nullable type?
    function (t) { return t.name === createInputFieldType.name; });
};
var getUpdateInputDataTypeForList = function (updateModifier, context) {
    var updateListModifierType = updateModifier.type;
    var updateInputFieldType = (0, getFinalType_1.default)(updateListModifierType.ofType);
    var updateListInputDataType = context.introspectionResults.types.find(function (introdspectionType) {
        return introdspectionType.name === updateInputFieldType.name;
    }).inputFields.find(function (input) { return input.name === "data"; })
        .type;
    var updateListInputDataName = (updateListInputDataType.kind === "NON_NULL"
        ? updateListInputDataType.ofType
        : updateListInputDataType).name;
    return context.introspectionResults.types.find(function (introdspectionType) { return introdspectionType.name === updateListInputDataName; });
};
function isObjectWithId(data) {
    if (!data)
        return false;
    if (!(0, isObject_1.default)(data))
        return false;
    return "id" in data;
}
var isNullReferenceObject = function (data) {
    return isObjectWithId(data) && (data.id === null || data.id === "");
};
var buildNewInputValue = function (fieldData, previousFieldData, fieldName, fieldType, context) {
    var kind = fieldType.kind;
    switch (kind) {
        case "SCALAR":
        case "ENUM": {
            // if its a date, convert it to a date
            if (fieldType.kind === "SCALAR" && fieldType.name === "DateTime") {
                return new Date(fieldData);
            }
            if (fieldType.kind === "SCALAR" && fieldType.name === "String") {
                if ((0, isObject_1.default)(fieldData)) {
                    return JSON.stringify(fieldData);
                }
            }
            return fieldData;
        }
        case "INPUT_OBJECT": {
            var fieldObjectType_1 = fieldType;
            var fullFieldObjectType = context.introspectionResults.types.find(function (t) { return t.name === fieldObjectType_1.name; });
            var setModifier = fullFieldObjectType === null || fullFieldObjectType === void 0 ? void 0 : fullFieldObjectType.inputFields.find(function (i) { return i.name === ModifiersParams.set; });
            var connectModifier = fullFieldObjectType === null || fullFieldObjectType === void 0 ? void 0 : fullFieldObjectType.inputFields.find(function (i) { return i.name === ModifiersParams.connect; });
            var disconnectModifier = fullFieldObjectType === null || fullFieldObjectType === void 0 ? void 0 : fullFieldObjectType.inputFields.find(function (i) { return i.name === ModifiersParams.disconnect; });
            var deleteModifier = fullFieldObjectType === null || fullFieldObjectType === void 0 ? void 0 : fullFieldObjectType.inputFields.find(function (i) { return i.name === ModifiersParams.delete; });
            var removeRelationMode = disconnectModifier
                ? "disconnect"
                : deleteModifier
                    ? "delete"
                    : null;
            if (setModifier &&
                !connectModifier &&
                !disconnectModifier &&
                !deleteModifier) {
                // if its a date, convert it to a date
                if (setModifier.type.kind === "SCALAR" &&
                    setModifier.type.name === "DateTime") {
                    return { set: new Date(fieldData) };
                }
                return { set: fieldData };
            }
            var isRelationship = fullFieldObjectType === null || fullFieldObjectType === void 0 ? void 0 : fullFieldObjectType.inputFields.every(function (i) {
                return Object.keys(ModifiersParams).includes(i.name);
            });
            // is it a relation?
            if (isRelationship) {
                // if it has a set modifier, it is an update array
                var createModifier = fullFieldObjectType === null || fullFieldObjectType === void 0 ? void 0 : fullFieldObjectType.inputFields.find(function (i) { return i.name === ModifiersParams.create; });
                var updateModifier = fullFieldObjectType === null || fullFieldObjectType === void 0 ? void 0 : fullFieldObjectType.inputFields.find(function (i) { return i.name === ModifiersParams.update; });
                var isList = fullFieldObjectType === null || fullFieldObjectType === void 0 ? void 0 : fullFieldObjectType.inputFields.some(function (i) {
                    return i.type.kind === "LIST";
                });
                if (isList) {
                    if (Array.isArray(fieldData)) {
                        var createListInputType_1 = getCreateInputDataTypeForList(createModifier, context);
                        var updateListInputType_1 = updateModifier
                            ? getUpdateInputDataTypeForList(updateModifier, context)
                            : null;
                        var variables = fieldData.reduce(function (inputs, referencedField) {
                            if ((0, isObject_1.default)(referencedField)) {
                                // TODO: we assume "data.id" to be the id
                                if (isObjectWithId(referencedField)) {
                                    var connectRelation = !(previousFieldData === null || previousFieldData === void 0 ? void 0 : previousFieldData.find(function (p) {
                                        return p.id
                                            ? p.id === referencedField.id
                                            : p === referencedField.id;
                                    }));
                                    if (!updateListInputType_1 || connectRelation) {
                                        inputs.connect = __spreadArray(__spreadArray([], (inputs.connect || []), true), [
                                            { id: referencedField.id },
                                        ], false);
                                    }
                                    else {
                                        // update
                                        var data = (0, exports.buildData)(updateListInputType_1, {
                                            id: referencedField.id,
                                            data: referencedField,
                                            previousData: previousFieldData === null || previousFieldData === void 0 ? void 0 : previousFieldData.find(function (previousField) {
                                                // TODO: we assume ".id" to be the id
                                                return previousField.id === referencedField.id;
                                            }),
                                        }, context);
                                        if (Object.keys(data).length) {
                                            inputs.update = __spreadArray(__spreadArray([], (inputs.update || []), true), [
                                                { where: { id: referencedField.id }, data: data },
                                            ], false);
                                        }
                                    }
                                }
                                else {
                                    // create
                                    var data = (0, exports.buildData)(createListInputType_1, {
                                        data: referencedField,
                                    }, context);
                                    inputs.create = __spreadArray(__spreadArray([], (inputs.create || []), true), [data], false);
                                }
                            }
                            else {
                                // only reference id's
                                // what about multiple id's for one reference?
                                if (!(previousFieldData === null || previousFieldData === void 0 ? void 0 : previousFieldData.find(function (p) {
                                    // TODO: we assume "p.id" to be the id
                                    return p.id
                                        ? p.id === referencedField
                                        : p === referencedField;
                                }))) {
                                    inputs.connect = __spreadArray(__spreadArray([], (inputs.connect || []), true), [
                                        { id: referencedField },
                                    ], false);
                                }
                            }
                            return inputs;
                        }, {});
                        // disconnect the ones that are not referenced anymore
                        if (Array.isArray(previousFieldData)) {
                            var removableRelations = previousFieldData.reduce(function (inputs, data) {
                                // TODO: we assume "data.id" to be the id
                                var dataId = data.id || data;
                                if (!fieldData.find(function (p) {
                                    // TODO: we assume "p.id" to be the id
                                    var pId = p.id || p;
                                    return pId === dataId;
                                })) {
                                    return __spreadArray(__spreadArray([], (inputs || []), true), [{ id: dataId }], false);
                                }
                                return inputs;
                            }, []);
                            if (removableRelations.length) {
                                if (removeRelationMode === "disconnect") {
                                    variables.disconnect = removableRelations;
                                }
                                else if (removeRelationMode === "delete") {
                                    variables.delete = removableRelations;
                                }
                            }
                        }
                        return variables;
                    }
                    else {
                        throw new Error(fieldName + " should be an array");
                    }
                }
                else {
                    // disconnect if field data is empty or if its a {id: null} or {id: ""} object
                    if (!fieldData || isNullReferenceObject(fieldData)) {
                        if (removeRelationMode === "disconnect") {
                            return {
                                disconnect: true,
                            };
                        }
                        else if (removeRelationMode === "delete") {
                            return {
                                delete: true,
                            };
                        }
                        // else skip it
                        return;
                    }
                    if ((0, isObject_1.default)(fieldData)) {
                        if (!isObjectWithId(fieldData)) {
                            if (!createModifier) {
                                return;
                            }
                            // TODO: we assume ".id" to be the id
                            var createObjectModifierType_1 = (0, getFinalType_1.default)(createModifier.type);
                            var createObjectInputType = context.introspectionResults.types.find(function (t) { return t.name === createObjectModifierType_1.name; });
                            // create
                            var data = (0, exports.buildData)(createObjectInputType, {
                                data: fieldData,
                            }, context);
                            return { create: data };
                        }
                        else {
                            if ((previousFieldData === null || previousFieldData === void 0 ? void 0 : previousFieldData.id) === fieldData.id) {
                                if (!updateModifier) {
                                    return;
                                }
                                var updateObjectModifierType_1 = (0, getFinalType_1.default)(updateModifier.type);
                                var updateObjectInputType = context.introspectionResults.types.find(function (t) { return t.name === updateObjectModifierType_1.name; });
                                // update
                                var data = (0, exports.buildData)(updateObjectInputType, {
                                    id: fieldData.id,
                                    data: fieldData,
                                    previousData: previousFieldData,
                                }, context);
                                return { update: data };
                            }
                            else if (connectModifier) {
                                return { connect: { id: fieldData.id } };
                            }
                        }
                    }
                    else if (connectModifier) {
                        return { connect: { id: fieldData } };
                    }
                }
            }
            else {
                return fieldData;
            }
            return;
        }
        case "LIST":
        case "NON_NULL":
            return;
        default:
            (0, exhaust_1.default)(kind);
    }
};
var buildData = function (inputType, params, context) {
    if (!inputType) {
        return {};
    }
    var data = params.data;
    var previousData = "previousData" in params ? params.previousData : null;
    var allKeys = (0, lodash_1.uniq)(__spreadArray(__spreadArray([], Object.keys(data), true), Object.keys(previousData !== null && previousData !== void 0 ? previousData : {}), true));
    // we only deal with the changedData set
    var changedData = Object.fromEntries(allKeys
        .filter(function (key) {
        if (!previousData) {
            return true;
        }
        var value = data[key];
        var previousValue = previousData[key];
        if ((0, isEqual_1.default)(value, previousValue)) {
            return false; // remove
        }
        if ((0, isNil_1.default)(value) && (0, isNil_1.default)(previousValue)) {
            return false;
        }
        return true;
    })
        .map(function (key) { return [key, data[key]]; }));
    return inputType.inputFields.reduce(function (acc, field) {
        var _a;
        // ignore unchanged field
        if (!(0, sanitizeData_1.hasFieldData)(changedData, field)) {
            return acc;
        }
        var fieldType = field.type.kind === "NON_NULL" ? field.type.ofType : field.type;
        // we have to handle the convenience convention that adds _id(s)  to the data
        // the sanitize function merges that with other data
        var _b = (0, sanitizeData_1.getSanitizedFieldData)(changedData, previousData, field), fieldData = _b.fieldData, previousFieldData = _b.previousFieldData;
        var newVaue = buildNewInputValue(fieldData, previousFieldData, field.name, fieldType, context);
        var key = field.name;
        return __assign(__assign({}, acc), (_a = {}, _a[key] = newVaue, _a));
    }, {});
};
exports.buildData = buildData;
//# sourceMappingURL=buildData.js.map