import { IntrospectionResult, Resource } from "../constants/interfaces";
import { OurOptions } from "../types";
export declare type BuildVariablesContext = {
    introspectionResults: IntrospectionResult;
    options: OurOptions;
    resource: Resource;
};
//# sourceMappingURL=types.d.ts.map