"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildVariables = void 0;
var isNil_1 = __importDefault(require("lodash/isNil"));
var isObject_1 = __importDefault(require("lodash/isObject"));
var react_admin_1 = require("react-admin");
var buildWhere_1 = require("./buildWhere");
var buildData_1 = require("./buildData");
var buildOrderBy_1 = require("./buildOrderBy");
var buildGetListVariables = function (context) {
    return function (aorFetchType, params) {
        var where = (0, buildWhere_1.buildWhere)(params.filter, context, aorFetchType);
        return {
            skip: (params.pagination.page - 1) * params.pagination.perPage,
            take: params.pagination.perPage,
            orderBy: (0, buildOrderBy_1.buildOrderBy)(params === null || params === void 0 ? void 0 : params.sort, context),
            where: where,
            aorFetchType: aorFetchType,
        };
    };
};
var getId = function (context, params) {
    var _a, _b;
    var id = (_a = params.id) !== null && _a !== void 0 ? _a : (_b = params.data) === null || _b === void 0 ? void 0 : _b.id;
    var type = context.introspectionResults.types.find(function (t) { return t.name === context.resource.type.name + "WhereUniqueInput"; });
    if (!type || type.kind !== "INPUT_OBJECT")
        return id;
    var idType = type.inputFields.find(function (f) { return f.name === "id"; });
    if (!idType || idType.type.kind !== "SCALAR")
        return id;
    if (idType.type.name === "String") {
        id = String(id);
    }
    if (idType.type.name === "Int") {
        id = parseInt(String(id));
    }
    // should usually not happen
    return id;
};
var buildGetOneVariables = function (context) { return function (params) {
    return {
        where: {
            id: getId(context, params),
        },
    };
}; };
var buildUpdateVariables = function (context) {
    return function (params, parentResource) {
        var _a, _b, _c, _d, _e, _f;
        var inputType = context.introspectionResults.types.find(function (t) { return t.name === context.resource.type.name + "UpdateInput"; });
        var id = getId(context, params); // TODO: do we still need params.data.id?
        delete params.data.id;
        (_a = params.previousData) === null || _a === void 0 ? true : delete _a.id;
        var data = (0, buildData_1.buildData)(inputType, params, context);
        return {
            where: {
                id: id,
            },
            data: (_f = (_e = (_d = (_c = (_b = context.options) === null || _b === void 0 ? void 0 : _b.customizeInputData) === null || _c === void 0 ? void 0 : _c[context.resource.type.name]) === null || _d === void 0 ? void 0 : _d.update) === null || _e === void 0 ? void 0 : _e.call(_d, data, params.data)) !== null && _f !== void 0 ? _f : data,
        };
    };
};
var buildCreateVariables = function (context) {
    return function (params, parentResource) {
        var _a, _b, _c, _d, _e;
        var inputType = context.introspectionResults.types.find(function (t) { return t.name === context.resource.type.name + "CreateInput"; });
        var data = (0, buildData_1.buildData)(inputType, params, context);
        return {
            data: (_e = (_d = (_c = (_b = (_a = context.options) === null || _a === void 0 ? void 0 : _a.customizeInputData) === null || _b === void 0 ? void 0 : _b[context.resource.type.name]) === null || _c === void 0 ? void 0 : _c.create) === null || _d === void 0 ? void 0 : _d.call(_c, data, params.data)) !== null && _e !== void 0 ? _e : data,
        };
    };
};
var buildVariables = function (context) {
    return function (aorFetchType, params) {
        var _a;
        switch (aorFetchType) {
            case react_admin_1.GET_LIST: {
                return buildGetListVariables(context)(aorFetchType, params);
            }
            case react_admin_1.GET_MANY:
                return {
                    where: {
                        id: {
                            in: params.ids
                                .map(function (obj) { return ((0, isObject_1.default)(obj) ? obj.id : obj); })
                                .filter(function (v) { return !(0, isNil_1.default)(v); }),
                        },
                    },
                };
            case react_admin_1.GET_MANY_REFERENCE: {
                return buildGetListVariables(context)(react_admin_1.GET_LIST, __assign(__assign({}, params), { filter: (_a = {}, _a[params.target] = params.id, _a) }));
            }
            case react_admin_1.GET_ONE: {
                return buildGetOneVariables(context)(params);
            }
            case react_admin_1.UPDATE: {
                return buildUpdateVariables(context)(params);
            }
            case react_admin_1.CREATE: {
                return buildCreateVariables(context)(params);
            }
            case react_admin_1.DELETE:
                return {
                    where: { id: params.id },
                };
        }
    };
};
exports.buildVariables = buildVariables;
//# sourceMappingURL=index.js.map