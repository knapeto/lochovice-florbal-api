"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isOneAndManyFragment = exports.isDeprecatedDocumentNodeFragment = void 0;
var isDeprecatedDocumentNodeFragment = function (fragment) {
    return "kind" in fragment && fragment.kind === "Document";
};
exports.isDeprecatedDocumentNodeFragment = isDeprecatedDocumentNodeFragment;
var isOneAndManyFragment = function (fragment) {
    return "one" in fragment || "many" in fragment;
};
exports.isOneAndManyFragment = isOneAndManyFragment;
//# sourceMappingURL=types.js.map