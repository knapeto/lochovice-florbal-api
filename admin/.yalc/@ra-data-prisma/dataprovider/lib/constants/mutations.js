"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PRISMA_UPDATE = exports.PRISMA_DELETE = exports.PRISMA_CREATE = exports.PRISMA_DISCONNECT = exports.PRISMA_CONNECT = void 0;
exports.PRISMA_CONNECT = "connect";
exports.PRISMA_DISCONNECT = "disconnect";
exports.PRISMA_CREATE = "create";
exports.PRISMA_DELETE = "delete";
exports.PRISMA_UPDATE = "update";
//# sourceMappingURL=mutations.js.map