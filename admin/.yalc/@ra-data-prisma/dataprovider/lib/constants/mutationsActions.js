"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PRISMA_UPDATED = exports.PRISMA_REMOVED = exports.PRISMA_NEW = void 0;
exports.PRISMA_NEW = "new";
exports.PRISMA_REMOVED = "removed";
exports.PRISMA_UPDATED = "updated";
//# sourceMappingURL=mutationsActions.js.map