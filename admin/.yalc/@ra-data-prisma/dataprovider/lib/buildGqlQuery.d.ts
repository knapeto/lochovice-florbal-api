import { ArgumentNode, DocumentNode, FieldNode, IntrospectionField, VariableDefinitionNode } from "graphql";
import { IntrospectionResult, Resource } from "./constants/interfaces";
import { FetchType, ResourceFragment } from "./types";
export interface Query {
    name?: string;
    args: IntrospectionField[];
}
export declare const buildFields: (introspectionResults: IntrospectionResult) => (fields: IntrospectionField[], maxNestingDepth?: number, depth?: number) => FieldNode[];
export declare const getArgType: (arg: IntrospectionField) => import("graphql").NamedTypeNode | import("graphql").ListTypeNode | import("graphql").NonNullTypeNode;
export declare const buildArgs: (query: Query, variables?: {
    [key: string]: any;
}) => ArgumentNode[];
export declare const buildApolloArgs: (query: Query, variables?: {
    [key: string]: any;
}) => VariableDefinitionNode[];
declare const _default: (introspectionResults: IntrospectionResult, options?: import("./types").OurOptions) => (resource: Resource, aorFetchType: FetchType, variables: {
    [key: string]: any;
}, fragment?: ResourceFragment | null) => DocumentNode;
export default _default;
//# sourceMappingURL=buildGqlQuery.d.ts.map