"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makePrefixedFullName = void 0;
var upperFirst_1 = __importDefault(require("lodash/upperFirst"));
var makePrefixedFullName = function (name, prefix) {
    return !prefix ? name : prefix + (0, upperFirst_1.default)(name);
};
exports.makePrefixedFullName = makePrefixedFullName;
//# sourceMappingURL=makePrefixedFullName.js.map