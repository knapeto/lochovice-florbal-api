"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeIntrospectionOptions = void 0;
var camelCase_1 = __importDefault(require("lodash/camelCase"));
var pluralize_1 = __importDefault(require("pluralize"));
var react_admin_1 = require("react-admin");
var makePrefixedFullName_1 = require("./makePrefixedFullName");
var makeIntrospectionOptions = function (options) {
    var _a, _b, _c;
    var prefix = function (s) { return (0, makePrefixedFullName_1.makePrefixedFullName)(s, options === null || options === void 0 ? void 0 : options.aliasPrefix); };
    var mutationOperationNames = __assign({ "nexus-prisma": (_a = {},
            _a[react_admin_1.CREATE] = function (resource) { return prefix("createOne" + resource.name); },
            _a[react_admin_1.UPDATE] = function (resource) { return prefix("updateOne" + resource.name); },
            _a[react_admin_1.DELETE] = function (resource) { return prefix("deleteOne" + resource.name); },
            _a), typegraphql: (_b = {},
            _b[react_admin_1.CREATE] = function (resource) { return prefix("create" + resource.name); },
            _b[react_admin_1.UPDATE] = function (resource) { return prefix("update" + resource.name); },
            _b[react_admin_1.DELETE] = function (resource) { return prefix("delete" + resource.name); },
            _b) }, options.mutationOperationNames);
    return __assign({ operationNames: __assign((_c = {}, _c[react_admin_1.GET_LIST] = function (resource) {
            return prefix("" + (0, pluralize_1.default)((0, camelCase_1.default)(resource.name)));
        }, _c[react_admin_1.GET_ONE] = function (resource) { return prefix("" + (0, camelCase_1.default)(resource.name)); }, _c[react_admin_1.GET_MANY] = function (resource) {
            return prefix("" + (0, pluralize_1.default)((0, camelCase_1.default)(resource.name)));
        }, _c[react_admin_1.GET_MANY_REFERENCE] = function (resource) {
            return prefix("" + (0, pluralize_1.default)((0, camelCase_1.default)(resource.name)));
        }, _c), mutationOperationNames[options.queryDialect]), exclude: undefined, include: undefined }, options.introspection);
};
exports.makeIntrospectionOptions = makeIntrospectionOptions;
//# sourceMappingURL=makeIntrospectionOptions.js.map