"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sanitizeKey = void 0;
var sanitizeKey = function (key) {
    if (key.endsWith("_ids")) {
        return key.substring(0, key.lastIndexOf("_ids"));
    }
    if (key.endsWith("_id")) {
        return key.substring(0, key.lastIndexOf("_id"));
    }
    return key;
};
exports.sanitizeKey = sanitizeKey;
//# sourceMappingURL=sanitizeKey.js.map