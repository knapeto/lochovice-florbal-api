"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function exhaust(value, message) {
    if (message === void 0) { message = "No such case in exhaustive switch: " + value; }
    throw new Error(message);
}
exports.default = exhaust;
//# sourceMappingURL=exhaust.js.map