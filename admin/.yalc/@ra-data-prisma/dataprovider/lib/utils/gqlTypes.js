"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.argument = exports.namedType = exports.name = exports.variable = exports.variableDefinition = exports.nonNullType = exports.listType = exports.field = exports.selectionSet = exports.operationDefinition = exports.document = void 0;
var graphql_1 = require("graphql");
// Functional utils to easily build GraphQL ASTs
// Inspired by https://github.com/imranolas/graphql-ast-types
var document = function (definitions) { return ({
    kind: graphql_1.Kind.DOCUMENT,
    definitions: definitions,
}); };
exports.document = document;
var operationDefinition = function (operation, selectionSet, name, variableDefinitions) { return ({
    kind: graphql_1.Kind.OPERATION_DEFINITION,
    operation: operation,
    selectionSet: selectionSet,
    name: name,
    variableDefinitions: variableDefinitions,
}); };
exports.operationDefinition = operationDefinition;
var selectionSet = function (selections) { return ({
    kind: graphql_1.Kind.SELECTION_SET,
    selections: selections,
}); };
exports.selectionSet = selectionSet;
var field = function (name, optionalValues) {
    if (optionalValues === void 0) { optionalValues = {}; }
    return (__assign({ kind: graphql_1.Kind.FIELD, name: name }, optionalValues));
};
exports.field = field;
var listType = function (type) { return ({
    kind: graphql_1.Kind.LIST_TYPE,
    type: type,
}); };
exports.listType = listType;
var nonNullType = function (type) { return ({
    kind: graphql_1.Kind.NON_NULL_TYPE,
    type: type,
}); };
exports.nonNullType = nonNullType;
var variableDefinition = function (variable, type) { return ({
    kind: graphql_1.Kind.VARIABLE_DEFINITION,
    variable: variable,
    type: type,
}); };
exports.variableDefinition = variableDefinition;
var variable = function (name) { return ({
    kind: graphql_1.Kind.VARIABLE,
    name: name,
}); };
exports.variable = variable;
var name = function (value) { return ({
    kind: graphql_1.Kind.NAME,
    value: value,
}); };
exports.name = name;
var namedType = function (name) { return ({
    kind: graphql_1.Kind.NAMED_TYPE,
    name: name,
}); };
exports.namedType = namedType;
var argument = function (name, value) { return ({
    kind: graphql_1.Kind.ARGUMENT,
    name: name,
    value: value,
}); };
exports.argument = argument;
//# sourceMappingURL=gqlTypes.js.map