import { DataProviderOptions } from "./types";
import { LegacyDataProvider } from "react-admin";
declare const useDataProvider: (options: DataProviderOptions) => LegacyDataProvider;
export default useDataProvider;
//# sourceMappingURL=useDataProvider.d.ts.map