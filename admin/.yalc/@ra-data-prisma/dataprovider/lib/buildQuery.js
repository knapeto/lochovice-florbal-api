"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildQueryFactory = void 0;
var react_admin_1 = require("react-admin");
var buildGqlQuery_1 = __importDefault(require("./buildGqlQuery"));
var buildVariables_1 = require("./buildVariables");
var getResponseParser_1 = __importDefault(require("./getResponseParser"));
var types_1 = require("./types");
var MANY_FETCH_TYPES = [react_admin_1.GET_LIST, react_admin_1.GET_MANY, react_admin_1.GET_MANY_REFERENCE];
var buildQueryFactory = function (introspectionResults, options) {
    var resourceViews = options.resourceViews;
    var knownResources = introspectionResults.resources.map(function (r) { return r.type.name; });
    return function (aorFetchType, resourceName, params) {
        var resourceView = resourceViews === null || resourceViews === void 0 ? void 0 : resourceViews[resourceName];
        var resourceNameToUse = resourceView
            ? resourceView.resource
            : resourceName;
        var resource = introspectionResults.resources.find(function (r) { return r.type.name === resourceNameToUse; });
        if (!resource) {
            throw new Error("Unknown resource " + resourceNameToUse + ". Make sure it has been declared on your server side schema. Known resources are " + knownResources.join(", ") + ". " + (resourceViews
                ? "Known view resources are " + Object.keys(resourceViews).join(", ")
                : ""));
        }
        var resourceViewFragment = undefined;
        if (resourceView) {
            if ((0, types_1.isOneAndManyFragment)(resourceView.fragment)) {
                // has one or many or both
                // if only one is defined, fall back to no fragment for the other
                if (MANY_FETCH_TYPES.includes(aorFetchType)) {
                    if (resourceView.fragment.many) {
                        resourceViewFragment = resourceView.fragment.many;
                    }
                }
                else {
                    if (resourceView.fragment.one) {
                        resourceViewFragment = resourceView.fragment.one;
                    }
                }
            }
            else {
                resourceViewFragment = resourceView.fragment;
            }
        }
        var variables = (0, buildVariables_1.buildVariables)({
            introspectionResults: introspectionResults,
            options: options,
            resource: resource,
        })(aorFetchType, params);
        if (options.queryDialect === "typegraphql") {
            Object.keys(variables).forEach(function (key) {
                var _a;
                variables[key] = (_a = variables[key]) !== null && _a !== void 0 ? _a : undefined;
            });
        }
        var query = (0, buildGqlQuery_1.default)(introspectionResults, options)(resource, aorFetchType, variables, resourceViewFragment);
        var parseResponse = (0, getResponseParser_1.default)(introspectionResults, {
            queryDialect: options.queryDialect,
        })(aorFetchType, resource);
        return {
            query: query,
            variables: variables,
            parseResponse: parseResponse,
        };
    };
};
exports.buildQueryFactory = buildQueryFactory;
//# sourceMappingURL=buildQuery.js.map