"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.makePrefixedFullName = exports.buildDataProvider = exports.useDataProvider = void 0;
var buildDataProvider_1 = __importDefault(require("./buildDataProvider"));
exports.buildDataProvider = buildDataProvider_1.default;
var makePrefixedFullName_1 = require("./utils/makePrefixedFullName");
Object.defineProperty(exports, "makePrefixedFullName", { enumerable: true, get: function () { return makePrefixedFullName_1.makePrefixedFullName; } });
var useDataProvider_1 = __importDefault(require("./useDataProvider"));
exports.useDataProvider = useDataProvider_1.default;
__exportStar(require("./types"), exports);
exports.default = buildDataProvider_1.default;
//# sourceMappingURL=index.js.map